#define PI 3.14159
#define NEG(nr) nr * (-1)

int main (void)
{
  double pi = PI;
  double minus_pi = NEG(pi);
  return 0;
}
