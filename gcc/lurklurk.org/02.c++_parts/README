Inspect the output of the g++ compiler on the sources located in src/ directory.

Function Overloading & Name Mangling

The first change that C++ allows is the ability to overload a function, so there can be different versions of the same named functions, differing in the types that the function accepts (the function's signature):

    int max(int x, int y) 
    { 
      if (x>y) return x; 
      else return y; 
    }
    float max(float x, float y) 
    { 
      if (x>y) return x; 
      else return y; 
    }
    double max(double x, double y) 
    { 
      if (x>y) return x; 
      else return y; 
    }

This obviously gives a problem for the linker: when some other code refers to max, which one does it mean?

The solution that was adopted for this is called name mangling, because all of the information about the function signature is mangled into a textual form, and that becomes the actual name of the symbol as seen by the linker. Different signature functions get mangled to different names, so the uniqueness problem goes away.

I'm not going to go into details of the schemes used (which vary from platform to platform anyway), but a quick look at the object file corresponding to the code above gives some hints (remember, nm is your friend!):



    Symbols from fn_overload.o:

    Name                  Value   Class        Type         Size     Line  Section

    __gxx_personality_v0|        |   U  |            NOTYPE|        |     |*UND*
    _Z3maxii            |00000000|   T  |              FUNC|00000021|     |.text
    _Z3maxff            |00000022|   T  |              FUNC|00000029|     |.text
    _Z3maxdd            |0000004c|   T  |              FUNC|00000041|     |.text

Here we can see that our three functions called max all get different names in the object files, and we can make a fairly shrewd guess that the two letters after the "max" are encoding the types of the parameters—"i" for int, "f" for float and "d" for double (things get a lot more complex when classes, namespaces, templates and overloaded operators get added into the mangling mix, though!).

It's also worth noting that there will normally be some way of converting between the user-visible names for things (the demangled names) and the linker-visible names for things (the mangled names). This might be a separate program (e.g. c++filt) or a command-line option (e.g. --demangle as an option to GNU nm), which gives results like:



    Symbols from fn_overload.o:

    Name                  Value   Class        Type         Size     Line  Section

    __gxx_personality_v0|        |   U  |            NOTYPE|        |     |*UND*
    max(int, int)            |00000000|   T  |              FUNC|00000021|     |.text
    max(float, float)            |00000022|   T  |              FUNC|00000029|     |.text
    max(double, double)            |0000004c|   T  |              FUNC|00000041|     |.text

The area where this mangling scheme most commonly trips people up is when C and C++ code is intermingled. All of the symbols produced by the C++ compiler are mangled; all of the symbols produced by the C compiler are just as they appear in the source file. To get around this, the C++ language allows you to put extern "C" around the declaration & definition of a function. This basically tells the C++ compiler that this particular name should not be mangled—either because it's the definition of a C++ function that some C code needs to call, or because it's a C function that some C++ code needs to call.

For the example given right at the start of this page, it's easy to see that there's a good chance someone has forgotten this extern "C" declaration in their link of C and C++ together.

    g++ -o test1 test1a.o test1b.o
    test1a.o(.text+0x18): In function `main':
    : undefined reference to `findmax(int, int)'
    collect2: ld returned 1 exit status

The big hint here is that the error message includes a function signature—it's not just complaining about plain old findmax missing. In other words, the C++ code is actually looking for something like "_Z7findmaxii" but only finding "findmax", and so it fails to link.

By the way, note that an extern "C" linkage declaration is ignored for member functions (7.5.4 of the C++ standard).

