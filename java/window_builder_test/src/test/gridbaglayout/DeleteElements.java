package test.gridbaglayout;

import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.border.TitledBorder;
import javax.swing.ButtonGroup;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DeleteElements extends JPanel {
  private final ButtonGroup buttonGroup = new ButtonGroup();
  private JTextField txtEmpty;
  private JTextField txtEmpty_1;
  public DeleteElements() {

    JPanel panel_1 = new JPanel();
    add(panel_1);
    panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.PAGE_AXIS));

    JPanel panel = new JPanel();
    panel_1.add(panel);
    panel.setBorder(new TitledBorder(null, "Options", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    GridBagLayout gbl_panel = new GridBagLayout();
    gbl_panel.columnWidths = new int[]{157, 0};
    gbl_panel.rowHeights = new int[]{0, 23, 0};
    gbl_panel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
    gbl_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
    panel.setLayout(gbl_panel);

    JRadioButton rdbtnDeleteFreeNodes = new JRadioButton("Delete Free Nodes");
    buttonGroup.add(rdbtnDeleteFreeNodes);
    GridBagConstraints gbc_rdbtnDeleteFreeNodes = new GridBagConstraints();
    gbc_rdbtnDeleteFreeNodes.anchor = GridBagConstraints.NORTHWEST;
    gbc_rdbtnDeleteFreeNodes.insets = new Insets(0, 0, 5, 0);
    gbc_rdbtnDeleteFreeNodes.gridx = 0;
    gbc_rdbtnDeleteFreeNodes.gridy = 0;
    panel.add(rdbtnDeleteFreeNodes, gbc_rdbtnDeleteFreeNodes);

    JRadioButton rdbtnKeepFreeNodes = new JRadioButton("Keep Free Nodes");
    buttonGroup.add(rdbtnKeepFreeNodes);
    GridBagConstraints gbc_rdbtnKeepFreeNodes = new GridBagConstraints();
    gbc_rdbtnKeepFreeNodes.anchor = GridBagConstraints.NORTHWEST;
    gbc_rdbtnKeepFreeNodes.gridx = 0;
    gbc_rdbtnKeepFreeNodes.gridy = 1;
    panel.add(rdbtnKeepFreeNodes, gbc_rdbtnKeepFreeNodes);

    JPanel panel_2 = new JPanel();
    panel_2.setBorder(new TitledBorder(null, "Elements Selection", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    panel_1.add(panel_2);
    GridBagLayout gbl_panel_2 = new GridBagLayout();
    gbl_panel_2.columnWidths = new int[]{113, 0, 0, 0};
    gbl_panel_2.rowHeights = new int[]{0, 15, 0};
    gbl_panel_2.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
    gbl_panel_2.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
    panel_2.setLayout(gbl_panel_2);

    JLabel lblSelectElements = new JLabel("Select Elements");
    GridBagConstraints gbc_lblSelectElements = new GridBagConstraints();
    gbc_lblSelectElements.insets = new Insets(0, 0, 5, 5);
    gbc_lblSelectElements.anchor = GridBagConstraints.EAST;
    gbc_lblSelectElements.gridx = 0;
    gbc_lblSelectElements.gridy = 0;
    panel_2.add(lblSelectElements, gbc_lblSelectElements);

    txtEmpty = new JTextField();
    txtEmpty.setText("empty");
    GridBagConstraints gbc_txtEmpty = new GridBagConstraints();
    gbc_txtEmpty.insets = new Insets(0, 0, 5, 5);
    gbc_txtEmpty.fill = GridBagConstraints.HORIZONTAL;
    gbc_txtEmpty.gridx = 1;
    gbc_txtEmpty.gridy = 0;
    panel_2.add(txtEmpty, gbc_txtEmpty);
    txtEmpty.setColumns(10);

    final JToggleButton tglbtnActivated = new JToggleButton("Activated");
    tglbtnActivated.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        boolean enabled = tglbtnActivated.isSelected();
        txtEmpty.setEnabled(enabled);
      }
    });
    GridBagConstraints gbc_tglbtnActivated = new GridBagConstraints();
    gbc_tglbtnActivated.fill = GridBagConstraints.HORIZONTAL;
    gbc_tglbtnActivated.insets = new Insets(0, 0, 5, 0);
    gbc_tglbtnActivated.gridx = 2;
    gbc_tglbtnActivated.gridy = 0;
    panel_2.add(tglbtnActivated, gbc_tglbtnActivated);

    JLabel lblSelectGeometry = new JLabel("Select Geometry");
    GridBagConstraints gbc_lblSelectGeometry = new GridBagConstraints();
    gbc_lblSelectGeometry.anchor = GridBagConstraints.EAST;
    gbc_lblSelectGeometry.insets = new Insets(0, 0, 0, 5);
    gbc_lblSelectGeometry.gridx = 0;
    gbc_lblSelectGeometry.gridy = 1;
    panel_2.add(lblSelectGeometry, gbc_lblSelectGeometry);

    txtEmpty_1 = new JTextField();
    txtEmpty_1.setText("Empty");
    GridBagConstraints gbc_txtEmpty_1 = new GridBagConstraints();
    gbc_txtEmpty_1.insets = new Insets(0, 0, 0, 5);
    gbc_txtEmpty_1.fill = GridBagConstraints.HORIZONTAL;
    gbc_txtEmpty_1.gridx = 1;
    gbc_txtEmpty_1.gridy = 1;
    panel_2.add(txtEmpty_1, gbc_txtEmpty_1);
    txtEmpty_1.setColumns(10);

    final JToggleButton toggleButton = new JToggleButton("Activated");
    toggleButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        boolean enabled = toggleButton.isSelected();
        txtEmpty_1.setEnabled(enabled);
      }
    });
    GridBagConstraints gbc_toggleButton = new GridBagConstraints();
    gbc_toggleButton.anchor = GridBagConstraints.EAST;
    gbc_toggleButton.gridx = 2;
    gbc_toggleButton.gridy = 1;
    panel_2.add(toggleButton, gbc_toggleButton);
  }

}
