package test.gridbaglayout;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JComboBox;
import java.awt.Insets;

import javax.swing.ComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.event.ListDataListener;
import java.awt.Color;
import javax.swing.border.LineBorder;

public class GridBagPanel extends JPanel {
  private JTextField textField;

  /**
   * Create the panel.
   */
  public GridBagPanel() {
    GridBagLayout gridBagLayout = new GridBagLayout();
    gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
//    gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
    gridBagLayout.columnWeights = new double[]{0.0, 1.0, 1.0, 0.0};
//    gridBagLayout.rowWeights = new double[]{1.0, 1.0, 0.0, Double.MIN_VALUE};
    setLayout(gridBagLayout);

    JPanel panel = new JPanel();
    panel.setBorder(new LineBorder(new Color(0, 0, 0)));
    panel.setBackground(Color.MAGENTA);
    GridBagConstraints gbc_panel = new GridBagConstraints();
    gbc_panel.anchor = GridBagConstraints.ABOVE_BASELINE_LEADING;
    gbc_panel.insets = new Insets(5, 5, 0, 5);
    gbc_panel.gridx = 0;
    gbc_panel.gridy = 0;
    add(panel, gbc_panel);

    ComboBoxModel<String> comboModel = new ComboBoxModel<String>() {
      String[] inputTypes = {"Name","Rgb", "Hls", "Hex code"};
      private String selectedInputType;
      @Override
      public int getSize() {
        return inputTypes.length;
      }

      @Override
      public String getElementAt(int index) {
        return inputTypes[index];
      }

      @Override
      public void addListDataListener(ListDataListener l) {
        // TODO Auto-generated method stub

      }

      @Override
      public void removeListDataListener(ListDataListener l) {
        // TODO Auto-generated method stub

      }

      @Override
      public void setSelectedItem(Object anItem) {
        selectedInputType = (String) anItem;
      }

      @Override
      public Object getSelectedItem() {
        return selectedInputType;
      }
    };
    JComboBox<String> comboBox = new JComboBox<String>(comboModel);
    GridBagConstraints gbc_comboBox = new GridBagConstraints();
    gbc_comboBox.insets = new Insets(0, 0, 0, 5);
    gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
    gbc_comboBox.gridx = 1;
    gbc_comboBox.gridy = 0;
    add(comboBox, gbc_comboBox);

    textField = new JTextField();
    GridBagConstraints gbc_textField = new GridBagConstraints();
    gbc_textField.insets = new Insets(0, 0, 0, 5);
    gbc_textField.fill = GridBagConstraints.HORIZONTAL;
    gbc_textField.gridx = 2;
    gbc_textField.gridy = 0;
    add(textField, gbc_textField);
    textField.setColumns(10);

    JButton button = new JButton("...");
    GridBagConstraints gbc_button = new GridBagConstraints();
    gbc_button.gridx = 3;
    gbc_button.gridy = 0;
    add(button, gbc_button);

  }

}
