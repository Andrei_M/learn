package test;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import test.gridbaglayout.DeleteElements;
import test.gridbaglayout.GridBagPanel;

public class MainTest {

  public static void main(String[] args) {
//    setupGridBagPanel();
    setupDeleteElements();
  }

  private static void setupGridBagPanel() {
    JFrame frame = new JFrame();
    JPanel panel = new GridBagPanel();
    frame.setContentPane(panel);

    setupFrame(frame);

    displayFrame(frame);
  }

  private static void setupDeleteElements()
  {
    JFrame frame = new JFrame();
    JPanel panel = new DeleteElements();
    frame.setContentPane(panel);
    setupFrame(frame);
    displayFrame(frame);
  }

  private static void setupFrame(JFrame frame) {
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  private static void displayFrame(final JFrame frame) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        frame.setVisible(true);
      }
    });
  }
}
