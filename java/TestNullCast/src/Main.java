
public class Main {
	public static void main(String[] args) {
		((Null)null).staticFoo(); // OK, works

		Null n = null;
		n.staticFoo(); // OK, works
	}
}
