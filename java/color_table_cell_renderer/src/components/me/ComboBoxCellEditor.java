package components.me;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;

public class ComboBoxCellEditor extends DefaultCellEditor{

  public ComboBoxCellEditor(JComboBox comboBox) {
    super(comboBox);
  }

}
