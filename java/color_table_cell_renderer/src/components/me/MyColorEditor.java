package components.me;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

public class MyColorEditor extends AbstractCellEditor
    implements TableCellEditor, ActionListener{
  private JButton editor;
  private Color currentColor = Color.gray;

  public MyColorEditor()
  {
    editor = new JButton("Color?");
    editor.addActionListener(this);
  }

  @Override
  public Object getCellEditorValue() {
    return currentColor;
  }

  @Override
  public Component getTableCellEditorComponent(JTable table, Object value,
      boolean isSelected, int row, int column) {
    return editor;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
  }
}
