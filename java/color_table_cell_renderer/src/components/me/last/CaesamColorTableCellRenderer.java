package components.me.last;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class CaesamColorTableCellRenderer implements TableCellRenderer {

  JButton myColoredLabel;
  Color myColor;

  int myInvocationCounter;

  CaesamColorTableCellRendererComponent myComponent;

  public CaesamColorTableCellRenderer(boolean b) {
    myColoredLabel = new JButton();
    myColor = Color.RED;
    myComponent = new CaesamColorTableCellRendererComponent();
    myInvocationCounter = 0;
  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object value,
      boolean isSelected, boolean hasFocus, int row, int column) {
    if (value != null) {
      myColor = (Color) value;
    } else {
      myColor = Color.RED;
    }

//    myColoredLabel.setText(colorToRgbString(myColor));
//    myColoredLabel.setBackground(myColor);
//    myColoredLabel.setForeground(createReadableColorOn(myColor));

    myComponent.setText(colorToRgbString(myColor));
    myComponent.setColor(myColor);
    myComponent.setBackground(table.getBackground());
    myComponent.setSelected(
        isSelected,
        table.getSelectionBackground(),
        table.getSelectionForeground(),
        table.getBackground());

//    myComponent.setTextColor(createReadableColorOn(myColor));

    return myComponent;
  }

  private static String colorToRgbString(Color color) {
    int red = color.getRed();
    int green = color.getGreen();
    int blue = color.getBlue();

    return "rgb("+red+","+green+","+blue+")";
  }

  private static Color createReadableColorOn(Color color) {
    return createBlackOrWhite(color);
  }

  private static Color createBlackOrWhite(Color color) {
    int red = color.getRed();
    int green = color.getGreen();
    int blue = color.getBlue();

    int gray = 255 - (red+green+blue)/3;

    if (gray < 128) {
      gray = 0;
    } else {
      gray  = 255;
    }


    return new Color(gray, gray, gray);
  }

}
