package components.me.last;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

class ColorComponent extends JPanel{

  private CircleColorChooser circleColorChooser;
  private Color myCurrentColor;
  JFrame colorChooserFrame = new JFrame();

  public ColorComponent(Color color)
  {
    myCurrentColor = color;
    circleColorChooser = new CircleColorChooser(myCurrentColor);

    colorChooserFrame = new JFrame();
    colorChooserFrame.setMinimumSize(new Dimension(150, 150));
    colorChooserFrame.setUndecorated(true);
    colorChooserFrame.setResizable(false);
    colorChooserFrame.pack();
    colorChooserFrame.setContentPane(circleColorChooser);
    colorChooserFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    colorChooserFrame.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        System.out.println("clicked");
        Window window = (Window) e.getSource();
        AWTEvent closeFrameEvent = new WindowEvent(window, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(closeFrameEvent);
      }
    });
    this.addMouseListener(new ColorComponentMouseListener());

  }

  public void setColor(Color theColor) {
    myCurrentColor = theColor;
    updateComponent();
  }

  private void updateComponent() {
    setBackground(myCurrentColor);
  }

  private class ColorComponentMouseListener extends MouseAdapter {

    @Override
    public void mouseClicked(MouseEvent e) {
      colorChooserFrame.setLocationRelativeTo(e.getComponent());
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          colorChooserFrame.setVisible(true);
        }
      });
    }
  }

  private class CircleColorChooser extends JPanel implements MouseListener {
    Color baseColor;

    public CircleColorChooser(Color color)
    {
      baseColor = color;
      setPreferredSize(new Dimension(400, 400));
      setOpaque(false);
      addMouseListener(this);
    }

    @Override
    public void paint(Graphics g) {
      super.paint(g);

      RenderingHints rh = new RenderingHints(
          RenderingHints.KEY_ANTIALIASING,
          RenderingHints.VALUE_ANTIALIAS_ON);
      rh.put(
          RenderingHints.KEY_RENDERING,
          RenderingHints.VALUE_RENDER_QUALITY);

      Graphics2D g2d = (Graphics2D) g;
      g2d.addRenderingHints(rh);

      int insets = 2;
      g.setColor(Color.BLACK);
      g.drawOval(insets, insets, getWidth()-2*insets, getHeight()-2*insets);
      int thirdWidth = getWidth()/3;
      int thirdHeight = getHeight();

      g.setColor(baseColor);
      g.drawOval(thirdWidth, thirdHeight, thirdWidth, thirdHeight);
      g.setClip(new Ellipse2D.Double(insets, insets, getWidth()-2*insets, getHeight()-2*insets));

      g.setColor(Color.BLUE);
      g.fillRect(0, 0, getWidth()/2, getHeight()/2);

      g.setColor(Color.YELLOW);
      g.fillRect(getWidth()/2, 0, getWidth(), getHeight()/2);

      g.setColor(Color.GREEN);
      g.fillRect(0, getHeight()/2, getWidth()/2, getHeight()/2);

      g.setColor(Color.RED);
      g.fillRect(getWidth()/2, getHeight()/2, getWidth()/2, getHeight()/2);

    }

    @Override
    public void mouseClicked(MouseEvent e) {
      System.out.println("panel clicked");
      JPanel panel = (JPanel) e.getSource();
      Window window = SwingUtilities.getWindowAncestor(panel);
      window.setVisible(false);
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
  }
}
