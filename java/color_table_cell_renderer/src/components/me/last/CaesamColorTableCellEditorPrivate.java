package components.me.last;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

class CaesamColorTableCellEditorPrivate extends JPanel {
  private Color myCurrentColor;
  private Color myChosenColor;

  private ColorComponent myColorComponent;
  private ColorTextField myColorTextField;
  private JButton        myMoreColorsButton;

  CaesamColorTableCellEditorPrivate(Color color) {
    this.setBackground(Color.GRAY);
    this.setLayout(new GridBagLayout());

    // Color (with constraints)
    myColorComponent = new ColorComponent(color);
    myColorComponent.setMinimumSize(new Dimension(20, 20));
    myColorComponent.setPreferredSize(new Dimension(20, 20));
    myColorComponent.setBackground(color);
    GridBagConstraints cc = new GridBagConstraints();
    cc.insets = new Insets(0, 0, 0, 5);
    // - add
    this.add(myColorComponent, cc);

    // text field (with constraints)
    myColorTextField = new ColorTextField(color);
    myColorTextField.setColorModel("rgb");
    GridBagConstraints tfc = new GridBagConstraints();
    tfc.weightx = 1.0;
    tfc.anchor = GridBagConstraints.LINE_START;
    // - add
    this.add(myColorTextField, tfc);

    // Advanced button (with constraints)
    myMoreColorsButton = new JButton("...");
    GridBagConstraints bc = new GridBagConstraints();
    bc.insets = new Insets(0, 5, 0, 0);
    // - add
    this.add(myMoreColorsButton, bc);
  }

  void setColor(Color theColor) {
    myCurrentColor = theColor;
    myColorComponent.setColor(theColor);
    myColorTextField.setColor(theColor);
  }
}
