package components.me.last;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class CaesamColorTableCellRendererComponent extends JPanel {
  private JPanel colorRect;
  private JLabel colorString;

  private Border selectedBorder;
  private Border unselectedBorder;

  public CaesamColorTableCellRendererComponent() {
    // layout
    this.setLayout(new GridBagLayout());

    // components
    colorRect = new JPanel();
//    colorRect.setPreferredSize(new Dimension(10, 10));
    colorRect.setAlignmentX(Component.LEFT_ALIGNMENT);
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.anchor = GridBagConstraints.LINE_START;
    constraints.gridx = 0;
    constraints.gridy = 0;
    constraints.insets = new Insets(0, 0, 0, 5);
    this.add(colorRect, constraints);

    colorString = new JLabel();
    constraints = new GridBagConstraints();
    constraints.gridx = 1;
    constraints.gridy = 0;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.LINE_END;
    this.add(colorString, constraints);
  }

  public void setText(String text) {
    colorString.setText(text);
  }

  public void setTextColor(Color color) {
    colorString.setForeground(color);
  }

  public void setColor(Color color) {
    colorRect.setBackground(color);
  }

  public void setSelected(
      boolean isSelected,
      Color selectedBackground,
      Color selectedForeground,
      Color unselectedBackground) {
    if (isSelected) {
      if (selectedBorder == null) {
          selectedBorder = BorderFactory.createMatteBorder(2,5,2,5,
                                    selectedBackground);
      }
      setBorder(selectedBorder);
      setBackground(selectedBackground);
    } else {
      if (unselectedBorder == null) {
          unselectedBorder = BorderFactory.createMatteBorder(2,5,2,5,
                                    unselectedBackground);
      }
      setBorder(unselectedBorder);
      setBackground(unselectedBackground);
    }
  }
};

