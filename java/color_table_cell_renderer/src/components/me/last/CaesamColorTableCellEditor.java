package components.me.last;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

public class CaesamColorTableCellEditor extends AbstractCellEditor
implements TableCellEditor{

  private CaesamColorTableCellEditorPrivate myPrivateEditor;
  private Color myCurrentColor;

  public CaesamColorTableCellEditor()
  {
    myCurrentColor = Color.GRAY;
  }

  @Override
  public Object getCellEditorValue() {
    return myCurrentColor;
  }

  @Override
  public Component getTableCellEditorComponent(JTable table, Object value,
      boolean isSelected, int row, int column) {
    myCurrentColor = (Color) value;

    if (myPrivateEditor == null) {
      myPrivateEditor = new CaesamColorTableCellEditorPrivate(myCurrentColor);
    } else {
      myPrivateEditor.setColor(myCurrentColor);
    }

    return myPrivateEditor;
  }

  private static int MIN_CLICK_COUNT_TO_EDIT = 2;

  @Override
  public boolean isCellEditable(EventObject e) {
    if (e instanceof MouseEvent) {
      MouseEvent me = (MouseEvent) e;
      return me.getClickCount() >= MIN_CLICK_COUNT_TO_EDIT;
    }
    return true;
  }

}
