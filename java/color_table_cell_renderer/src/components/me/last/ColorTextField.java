package components.me.last;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;

class ColorTextField extends JComponent implements ItemListener {
  private String[]          myColorModels;
  private JComboBox<String> myCurrentModel;
  private JTextField        myP1;
  private JTextField        myP2;
  private JTextField        myP3;

  private Color             myCurrentColor;

   ColorTextField(Color theColor) {
    myCurrentColor = theColor;
    myColorModels = new String[] { "rgb", "hsb" };
    myCurrentModel = new JComboBox<String>(myColorModels);

    myP1 = new JTextField(3);
    myP2 = new JTextField(3);
    myP3 = new JTextField(3);

    setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
    Box aBox = Box.createHorizontalBox();

    aBox.add(myCurrentModel);
    GridBagConstraints aConstraints = new GridBagConstraints();
    aConstraints.weightx = 1.0;
    aBox.add(myP1, aConstraints);
    aBox.add(myP2, aConstraints);
    aBox.add(myP3, aConstraints);

    add(aBox);
    myCurrentModel.addItemListener(this);
  }

   void setColorModel(String colorModelString) {
    int r = myCurrentColor.getRed();
    int g = myCurrentColor.getGreen();
    int b = myCurrentColor.getBlue();

    if ("rgb".equals(colorModelString)) {
      myCurrentModel.setSelectedIndex(0);
      myP1.setText("" + r);
      myP2.setText("" + g);
      myP3.setText("" + b);
    } else if ("hls".equals(colorModelString)) {
      myCurrentModel.setSelectedIndex(1);
      float[] hsb = new float[3];
      Color.RGBtoHSB(r, g, b, hsb);
      myP1.setText("" + hsb[0]);
      myP2.setText("" + hsb[1]);
      myP3.setText("" + hsb[2]);
    }
  }

  @Override
  public void itemStateChanged(ItemEvent e) {
    updateValues();
  }

   void setColor(Color theColor) {
    myCurrentColor = theColor;
    updateValues();
  }

  private void updateValues() {
    int index = myCurrentModel.getSelectedIndex();
    int r = myCurrentColor.getRed();
    int g = myCurrentColor.getGreen();
    int b = myCurrentColor.getBlue();

    if (index == 0) {
      setText(r, g, b);
    } else if (index == 1) {
      float[] hsb = new float[3];
      Color.RGBtoHSB(r, g, b, hsb);
      setText(hsb[0], hsb[1], hsb[2]);
    }
  }

  private void setText(int r, int g, int b) {
    myP1.setText("" + r);
    myP2.setText("" + g);
    myP3.setText("" + b);
  }

  private void setText(float h, float s, float b) {
    myP1.setText("" + h);
    myP2.setText("" + s);
    myP3.setText("" + b);
  }
}
