package test;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Main
{
  public static void main(String[] args)
  {
    SwingUtilities.invokeLater(new Runnable(){
      @Override
      public void run()
      {
        JFrame aFrame = new JFrame();
        TestBoxLayout aTestPanel = new TestBoxLayout(); 
        aFrame.setContentPane(aTestPanel);
        aFrame.pack();
        aFrame.setLocationRelativeTo(null);
        aFrame.setVisible(true);
      }
    });
  }
}
