package test;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JList;
import javax.swing.Box;
import java.awt.Component;
import javax.swing.AbstractListModel;

public class TestBoxLayout extends JPanel
{

  /**
   * Create the panel.
   */
  public TestBoxLayout() {
    Box aLeftBox = Box.createVerticalBox();
    JLabel aLeftLabel = new JLabel("Left Box");
    aLeftLabel.setAlignmentY(Component.TOP_ALIGNMENT);
    aLeftLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    aLeftBox.add(aLeftLabel);
    
    JList list = new JList(new AbstractListModel() {
      String[] values = new String[] {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5"};
      public int getSize() {
        return values.length;
      }
      public Object getElementAt(int index) {
        return values[index];
      }
    });
    aLeftBox.add(list);
    aLeftBox.add(Box.createVerticalGlue());

    Box aRightBox = Box.createVerticalBox();
    JLabel aRightLabel = new JLabel("RightBox");
    aRightLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    aRightLabel.setAlignmentY(Component.TOP_ALIGNMENT);
    aRightBox.add(aRightLabel);
    JList list2 = new JList(list.getModel());
    aRightBox.add(list2);
    aRightBox.add(Box.createVerticalGlue());

    Box aContentBox = Box.createHorizontalBox();
    aContentBox.add(aLeftBox);
    aContentBox.add(Box.createHorizontalGlue());
    aContentBox.add(aRightBox);
    
    add(aContentBox, BorderLayout.CENTER);
  }

}
