/**
 * This project will answer the following question:
 *   "Is it possible to continue to run a java app after the main thread ends?"
 * This should be possible if we switch execution to another thread. When the main
 * thread dies, the second thread should continue its execution, which does.
 * @author test
 *
 */
public class Main {

  static Runnable r = new Runnable() {
    @Override
    public void run() {
      System.out.println("In second Thread...");
      while (true) {
        System.out.print(".");
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }

  };

  public static void main(String[] args) {
    System.out.println("main:IN");
    Thread t = new Thread(r);
    t.start();
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    System.out.println("main:OUT");
  }
}
