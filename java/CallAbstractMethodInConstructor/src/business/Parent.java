package business;

public abstract class Parent {
	public Parent() {
		System.out.println("Parent.constructor:IN");
//		print();
		System.out.println("Parent.constructor:OUT");
	}

	public abstract void print();
}
