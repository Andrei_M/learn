package business;

public class Child extends Parent{
	private int myInt;
  private String myMessage;

	public Child() {
		System.out.println("Child.constructor:IN");
		myInt = 54;
    myMessage = "Hello, tricky";
		System.out.println("Child.constructor:OUT");
	}

	@Override
	public void print() {
		System.out.println("Child.print:" + this + "; myInt=" + myInt + "; a message: " + myMessage);
    //System.out.println("length=" + myMessage.length()); // ERROR
	}

}
