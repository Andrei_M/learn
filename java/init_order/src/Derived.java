
public class Derived extends Base{
  /* this field cannot be made non-static, as it is passed as an argument
   * to the supertype constructor */
  private static String CLASS_ID = "Derived";

  protected Derived(String theTitle) {
    super(CLASS_ID + "_" + theTitle);
  }
}
