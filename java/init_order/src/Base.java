
public class Base {
  private String myId;

  protected Base(String theId) {
    myId = theId;
  }

  @Override
  public String toString() {
    return myId;
  }
}
