package test;

import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.border.TitledBorder;
import javax.swing.AbstractListModel;

public class TestGridBag extends JPanel
{

  /**
   * Create the panel.
   */
  public TestGridBag() {
    setBorder(new TitledBorder(null, "Test GridBagLayout", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    setLayout(new GridLayout(1, 0, 0, 0));
    
    JLabel lblClassName = new JLabel("Class Name");
    add(lblClassName);
    
    JList list = new JList();
    list.setModel(new AbstractListModel() {
      String[] values = new String[] {"Item1", "Item2", "Item3", "Item4", "Item5", "Item6", "Item7"};
      public int getSize() {
        return values.length;
      }
      public Object getElementAt(int index) {
        return values[index];
      }
    });
    add(list);

  }

}
