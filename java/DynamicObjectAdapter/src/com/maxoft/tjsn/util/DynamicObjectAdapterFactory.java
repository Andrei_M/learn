package com.maxoft.tjsn.util;

import java.lang.reflect.Proxy;

public class DynamicObjectAdapterFactory {
  @SuppressWarnings("unchecked")
  public static <T> T adapt(final Object adaptee, final Class<T> target,
      final Object adapter) {

    T aProxyInstance = (T) Proxy.newProxyInstance(
        target.getClassLoader(),
        target.getInterfaces(), // instead of new Class[]{target}
        new MyInvocationHandler(adaptee, adapter));

    return aProxyInstance;
  }
}
