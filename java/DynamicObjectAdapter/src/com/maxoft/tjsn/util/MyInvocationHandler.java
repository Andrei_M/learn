package com.maxoft.tjsn.util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class MyInvocationHandler implements InvocationHandler {

  private Object adaptee;
  private Object adapter;

  private Map<MethodIdentifier, Method> adaptedMethods = new HashMap<MethodIdentifier, Method>();
  // initializer block - find all methods in adapter object
  {
    Method[] methods = adapter.getClass().getDeclaredMethods();
    for (Method m : methods) {
      adaptedMethods.put(new MethodIdentifier(m), m);
    }
  }

  public MyInvocationHandler(Object adaptee, Object adapter) {
    this.adaptee = adaptee;
    this.adapter = adapter;
  }

  public Object invoke(Object proxy, Method method, Object[] args)
      throws Throwable {
    try {
      Method other = adaptedMethods.get(new MethodIdentifier(method));
      if (other != null) {
        return other.invoke(adapter, args);
      } else {
        return method.invoke(adaptee, args);
      }
    } catch (InvocationTargetException e) {
      throw e.getTargetException();
    }
  }

}
