package org.learn.test;
import java.lang.reflect.Field;

import org.learn.Class1;

class TestReflection
{
    public static void main(String... args)
    {
        System.out.println("main:IN");
        Class1 aClass = new Class1();
        Field[] aClassFields = aClass.getClass().getFields();
        System.out.println("Class1 has: " + aClass.getClass().getFields().length + " fields.");
        for (Field field : aClassFields)
        {
          System.out.println("Field: " + field.getName());
          System.out.println("Field: " + field.getType());
          System.out.println("Field: " + field.toString());
          System.out.println("---------------------------");
        }
        System.out.println("main:OUT");
    }
}
