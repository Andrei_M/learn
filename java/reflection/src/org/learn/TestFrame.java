package org.learn;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import net.miginfocom.swing.MigLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;

@SuppressWarnings("serial")
public class TestFrame extends JFrame
{
  private JPanel contentPane;
  private JTextField textField;
  private final Action mySearchAction = new SearchSubTypeAction();
  private final Action myDisplayAction = new DisplayAction();

  /**
   * Launch the application.
   */
  public static void main(String[] args)
  {
    EventQueue.invokeLater(new Runnable() {
      public void run()
      {
        try {
          TestFrame frame = new TestFrame();
          frame.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the frame.
   */
  public TestFrame() {
    setAlwaysOnTop(true);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 450, 300);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    contentPane.setLayout(new BorderLayout(0, 0));
    setContentPane(contentPane);
    
    JPanel panel = new JPanel();
    contentPane.add(panel, BorderLayout.CENTER);
    MigLayout aLayout = new MigLayout("", "[][grow]", "[][][][grow]");
    panel.setLayout(aLayout);
    
    JLabel lblClassName = new JLabel("Class name");
    panel.add(lblClassName, "cell 0 0,alignx trailing");
    
    textField = new JTextField();
    panel.add(textField, "flowx,cell 1 0,grow");
    textField.setColumns(10);
    
    JButton btnSearch = new JButton("Search");
    btnSearch.setAction(mySearchAction);
    panel.add(btnSearch, "cell 1 0");
    
    JLabel lblMembers = new JLabel("Members");
    panel.add(lblMembers, "cell 0 2 2 1,alignx center,aligny center");
    
    JTree tree = new JTree(new DefaultTreeModel(new DefaultMutableTreeNode("Root"), false));
    tree.setRootVisible(false);
    tree.setRowHeight(30);
    JScrollPane aScrollPane = new JScrollPane(tree);
    panel.add(aScrollPane, "flowy,cell 0 3 2 1,grow");
    
    JButton btnDisplay = new JButton("Display");
    btnDisplay.setAction(myDisplayAction);
    panel.add(btnDisplay, "flowx,cell 0 4 2 1,alignx trailing,aligny baseline");
    JButton btnCancel = new JButton("Cancel");
    
    panel.add(btnCancel, "cell 1 4,alignx leading,aligny baseline");
    setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{lblClassName, textField, btnSearch, tree, aScrollPane, btnDisplay, btnCancel, contentPane, panel, lblMembers}));
  }

  private class SearchSubTypeAction extends AbstractAction {
    public SearchSubTypeAction() {
      putValue(ACTION_COMMAND_KEY, "Search");
      putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
      putValue(NAME, "Search");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      JOptionPane.showConfirmDialog(getContentPane(), "You have to press one more button", "Warning", JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE, null);

    }
  }
  private class DisplayAction extends AbstractAction {
    public DisplayAction() {
      putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));
      putValue(NAME, "Display");
      putValue(SHORT_DESCRIPTION, "Some short description");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }
  }
}
