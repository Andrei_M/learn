package test;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import scenario.TableDisplay;
import scenario.provider.SimpleTableProvider;
import scenario.renderer.SimpleTableCellRenderer;

public class MainTest {
  public static void main(String[] args) {
    simpleTableScenario();
  }

  private static void simpleTableScenario()
  {
    TableDisplay aTableDisplay = new TableDisplay();
    JTable aSimpleTable = new SimpleTableProvider().getTable();

    // set the cell renderer of column 0
    TableColumn aTableColumn = aSimpleTable.getColumn(1);
    aTableColumn.setCellRenderer(new SimpleTableCellRenderer());
    aTableDisplay.displayTable(aSimpleTable);
  }
}
