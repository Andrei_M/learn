package scenario.provider;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

public class SimpleTableProvider implements TableProvider {
  public JTable setupSimpleTable() {
    JTable aSimpleTable = new JTable();
    @SuppressWarnings("serial")
    TableModel aTableModel = new AbstractTableModel() {
      @Override
      public int getRowCount() {
        // TODO Auto-generated method stub
        return 10;
      }

      @Override
      public int getColumnCount() {
        // TODO Auto-generated method stub
        return 3;
      }

      @Override
      public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
          return rowIndex;
        } else {
          return "" + (rowIndex+1) + "," + (columnIndex+1);
        }
      }

      @Override
      public String getColumnName(int column) {
        return "" + (column+1) + ":" + super.getColumnName(column);
      }
    };
    aSimpleTable.setModel(aTableModel);

    return aSimpleTable;
  }


  @Override
  public JTable getTable() {
    return setupSimpleTable();
  }

}
