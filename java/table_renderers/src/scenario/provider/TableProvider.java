package scenario.provider;

import javax.swing.JTable;

public interface TableProvider {
  /**
   *
   * @return
   */
  JTable getTable();
}
