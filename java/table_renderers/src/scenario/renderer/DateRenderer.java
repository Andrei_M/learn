package scenario.renderer;

import java.text.DateFormat;

import javax.swing.table.DefaultTableCellRenderer;

public class DateRenderer extends DefaultTableCellRenderer {
  DateFormat formatter;
  public DateRenderer() { super(); }

  public void setValue(Object value) {
    if (formatter == null) {
      formatter = DateFormat.getDateInstance();
    }

    setText((value==null) ? "N/A" : formatter.format(value));
  }
}
