package scenario;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

public class TableDisplay {

  public void displayTable(JTable table)
  {
    final JFrame aFrame = new JFrame();
    JScrollPane aTableScrollPane = new JScrollPane(table);
    aTableScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    aFrame.add(aTableScrollPane);
    aFrame.pack();
    aFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    aFrame.setLocationRelativeTo(null);

    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        // TODO Auto-generated method stub
        aFrame.setVisible(true);
      }
    });
  }
}
