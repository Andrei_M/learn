class TemplateClass<T>
{
  public void add(T theArgument, int theInt)
  {
    p("TemplateClass.add<? extends T>");
  }
  
  public void addT(T theArgument)
  {
    p("TemplateClass.add<T>");
  }
  
  private void p(String theString)
  {
    System.out.println(theString);
  }
}
