package package2;

import package1.ParentClass;


public class ChildClass extends ParentClass
{
    public ChildClass()
    {
        super();
        System.out.println("ChildClass.ChildClass");

        myPrivateMethod();
    }
    
    @Override
    public void myPrivateMethod()
    {
        super.myPrivateMethod();
        System.out.println("ChildClass.myPrivateMethod");
    }
}
