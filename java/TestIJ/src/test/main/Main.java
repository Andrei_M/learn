package test.main;

import java.awt.Component;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import ij.ImageJ;

public class Main
{
  public static void main(String[] args)
  {
    ImageJ aImageJInstance = new ImageJ(ImageJ.EMBEDDED);
    
    final JFrame aFrame = new JFrame("My frame");
    JPanel aPanel = new JPanel();
    Component[] aComponents = aImageJInstance.getComponents();
    
    for (Component aComponent : aComponents) {
      aPanel.add(aComponent);
    }
    aFrame.setContentPane(aPanel);
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run()
      {
        aFrame.setVisible(true);
      }
    });
  }
}
