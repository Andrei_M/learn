package test;

import java.io.DataInputStream;
import java.io.InputStream;

public class TestOverwrite {
	public void foo(Object n, InputStream is) throws NoSuchMethodException, SecurityException{
		System.out.println("Object, InputStream");
	}

	public void foo(Float f, InputStream is) throws NoSuchMethodException, SecurityException {
		System.out.println("Float, InputStream");
	}

	public void foo(Object n, DataInputStream dis) throws NoSuchMethodException, SecurityException {
		System.out.println("Object, DataInputStream");
	}

	public static void main(String[] args) {
		TestOverwrite to = new TestOverwrite();
		try {
			to.foo(new Float(4), new DataInputStream(null));
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
