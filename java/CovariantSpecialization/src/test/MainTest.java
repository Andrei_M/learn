package test;

import money.Cash;
import money.SearsCard;
import store.McDonalds;
import store.Sears;
import store.Store;

public class MainTest {
	public static void main(String[] args) {
		Store mac = new McDonalds();
		mac.accept(new Cash());

		Store sears = new Sears();
		sears.accept(new SearsCard());
	}
}
