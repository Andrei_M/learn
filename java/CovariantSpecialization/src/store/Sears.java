package store;

import money.Cash;
import money.Money;
import money.SearsCard;

public class Sears extends Store{

	@Override
	public void accept(Money m) {
		super.accept(m);
		System.out.println("Sears accept money");
	}

	public void accept(Cash c) {
		System.out.println("Sears accept cash");
	}

	public void accept(SearsCard sc) {
		System.out.println("Sears accept sears card");
	}
}
