package store;

import java.lang.reflect.Method;

import money.Money;

public abstract class Store {
	public void accept(Money m) {
		Class<?> inherentReceiverType = this.getClass();
		Class<?>[] inherentArgTypes = { m.getClass(), };
		try {
			// this.getClass().getDeclaredMethod("accept", args);
			Method method = null; // current best method
			Method[] methods = inherentReceiverType.getMethods();

			for (Method candidateMethod : methods) {
				if ( ! (candidateMethod.getName().equals("accept"))) {
					// skip
					continue;
				}
				Class<?> paramTypes[] = candidateMethod.getParameterTypes();

				// check if method is applicable
				if (paramTypes.length != inherentArgTypes.length) {
					// skip
					continue;
				}

				for (int j = 0; j < inherentArgTypes.length; ++j) {
					if ( ! (paramTypes[j]
							.isAssignableFrom(inherentArgTypes[j]))) {
						// abort current candidate method
						break;
					}

					// check if current method is more specific
					if (method == null
							|| (method.getParameterTypes()[0])
									.isAssignableFrom(paramTypes[0])) {
						method = candidateMethod;
					}
				}
			}

			if (method.getParameterTypes()[0] == Money.class) {
				  // selected method is current method, so throw exception to prevent recursion
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		}

	}
}
