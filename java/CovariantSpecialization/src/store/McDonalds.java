package store;

import money.Cash;
import money.Money;
import money.VisaCard;

public class McDonalds extends Store {

	@Override
	public void accept(Money m) {
		super.accept(m);
		System.out.println("McDonalds accept money");
	}

	public void accept(Cash c) {
		System.out.println("McDonalds accept cash");
	}

	public void accept(VisaCard vc) {
		System.out.println("McDonalds accept visa card");
	}
}
