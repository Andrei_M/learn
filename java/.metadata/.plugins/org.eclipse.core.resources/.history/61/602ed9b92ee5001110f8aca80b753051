package test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MainTest {
  public static void main(String[] args) {
    JComponent contentPane = createContentPane();
    JComponent component = createComponent(Color.CYAN);

    contentPane.add(component);

    createAndDisplayFrame(contentPane);
  }

  private static JComponent createContentPane() {
    JComponent contentPane = new JPanel();
    contentPane.setBackground(Color.DARK_GRAY);
    contentPane.setLayout(new FlowLayout());
    return contentPane;
  }

  private static JComponent createComponent(Color color)
  {
    final JPanel component = new JPanel();
    component.setBackground(Color.GRAY);
//    component.setPreferredSize(new Dimension(500,100));

    component.setLayout(new GridBagLayout());

    // Color (with constraints)
    JComponent colorComp = new JPanel();
    colorComp.setBackground(color);
    GridBagConstraints cc = new GridBagConstraints();
    cc.gridx = 0;
    cc.gridy = 0;
    cc.gridwidth=1;
    cc.gridheight=1;
    cc.anchor = GridBagConstraints.LINE_START;
    cc.insets = new Insets(0, 5, 0, 5);
    // - add
    component.add(colorComp, cc);

    // text field (with constraints)
    JTextField tf = new JTextField(toRgbString(color));
    tf.setColumns(20);
    GridBagConstraints tfc = new GridBagConstraints();
    tfc.gridx=1;
    tfc.gridy=0;
    tfc.gridwidth=3;
    tfc.gridheight=1;
    tfc.anchor = GridBagConstraints.CENTER;
    tfc.fill = GridBagConstraints.HORIZONTAL;

    // - add
    component.add(tf, tfc);

    // Advanced button (with constraints)
    JButton b = new JButton("...");
    GridBagConstraints bc = new GridBagConstraints();
    bc.gridx=4;
    bc.gridy=0;
    bc.gridwidth=1;
    bc.gridheight=1;
    bc.anchor = GridBagConstraints.LINE_END;
    bc.insets = new Insets(10,10,10,10);
    // - add
    component.add(b, bc);

    return component;
  }

  private static String toRgbString(Color color) {
    int red = color.getRed();
    int green = color.getGreen();
    int blue = color.getBlue();

    return "rgb(" + red + "," + green + "," + blue + ")";
  }

  private static void createAndDisplayFrame(final JComponent contentPane) {
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        JFrame frame=  new JFrame();
        frame.setContentPane(contentPane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
      }
    });
  }
}
