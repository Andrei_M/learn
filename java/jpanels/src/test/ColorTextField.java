package test;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;

public class ColorTextField extends JComponent
implements ItemListener{
  private JComboBox<String> colorModel;
  private JTextField p1;
  private JTextField p2;
  private JTextField p3;

  private Color currentColor;

  public ColorTextField(Color color)
  {
    currentColor = color;
    String[] colorModels = new String[] {"rgb", "hsb"};
    colorModel =  new JComboBox(colorModels);

    p1 = new JTextField(3);
    p2 = new JTextField(3);
    p3 = new JTextField(3);

    setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
    Box b = Box.createHorizontalBox();

    b.add(colorModel);
    GridBagConstraints c = new GridBagConstraints();
    c.weightx=1.0;
    b.add(p1, c);
    b.add(p2, c);
    b.add(p3, c);

    add(b);
    colorModel.addItemListener(this);
  }

  public void setColorModel(String colorModelString) {
    int r = currentColor.getRed();
    int g = currentColor.getGreen();
    int b = currentColor.getBlue();

    if ("rgb".equals(colorModelString)) {
      colorModel.setSelectedIndex(0);
      p1.setText("" + r);
      p2.setText("" + g);
      p3.setText("" + b);
    } else if ("hls".equals(colorModelString)) {
      colorModel.setSelectedIndex(1);
      float[] hsb = new float[3];
      Color.RGBtoHSB(r, g, b, hsb);
      p1.setText(""+hsb[0]);
      p2.setText(""+hsb[1]);
      p3.setText(""+hsb[2]);
    }
  }

  @Override
  public void itemStateChanged(ItemEvent e) {

    int index = colorModel.getSelectedIndex();
    int r = currentColor.getRed();
    int g = currentColor.getGreen();
    int b = currentColor.getBlue();

    if (index == 0) {
      p1.setText("" + r);
      p2.setText("" + g);
      p3.setText("" + b);
    } else if (index == 1) {
      float[] hsb = new float[3];
      Color.RGBtoHSB(r, g, b, hsb);
      p1.setText(""+hsb[0]);
      p2.setText(""+hsb[1]);
      p3.setText(""+hsb[2]);
    }
  }
}
