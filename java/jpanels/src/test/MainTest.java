package test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MainTest {
  public static void main(String[] args) {
    JComponent contentPane = createContentPane();
    JComponent component = createColorComponent(Color.CYAN);

    contentPane.setLayout(new GridBagLayout());
    GridBagConstraints cc = new GridBagConstraints();
    cc.gridx      = 0;
    cc.gridy      = 0;
    cc.gridwidth  = 1;
    cc.gridheight = 1;
    cc.weightx    = 1;
    cc.weighty    = 1;
    cc.anchor     = GridBagConstraints.CENTER;
    cc.fill       = GridBagConstraints.BOTH;
    cc.insets     = new Insets(0, 0, 0, 0);
    cc.ipadx      = 0;
    cc.ipady      = 0;
    contentPane.add(component, cc);


    createAndDisplayFrame(contentPane);
  }

  private static JComponent createContentPane() {
    JComponent contentPane = new JPanel();
    contentPane.setBackground(Color.DARK_GRAY);
    return contentPane;
  }

  private static JComponent createColorComponent(Color color)
  {
    final JPanel component = new JPanel();
    component.setBackground(Color.GRAY);
    component.setLayout(new GridBagLayout());

    // Color (with constraints)
    JComponent colorComp = new ColorComponent(color);
    colorComp.setMinimumSize(new Dimension(20, 20));
    colorComp.setPreferredSize(new Dimension(20, 20));
    colorComp.setBackground(color);
    GridBagConstraints cc = new GridBagConstraints();
    cc.insets = new Insets(0,0,0,5);
    // - add
    component.add(colorComp, cc);

    // text field (with constraints)
    ColorTextField tf = new ColorTextField(color);
    tf.setColorModel("rgb");
    GridBagConstraints tfc = new GridBagConstraints();
    tfc.weightx = 1.0;
    tfc.anchor = GridBagConstraints.LINE_START;
    // - add
    component.add(tf, tfc);

    // Advanced button (with constraints)
    JButton b = new JButton("...");
    GridBagConstraints bc = new GridBagConstraints();
    bc.insets = new Insets(0,5,0,0);
    // - add
    component.add(b, bc);

    return component;
  }

  private static String toRgbString(Color color) {
    int red = color.getRed();
    int green = color.getGreen();
    int blue = color.getBlue();

    return "rgb(" + red + "," + green + "," + blue + ")";
  }

  private static void createAndDisplayFrame(final JComponent contentPane) {
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        JFrame frame=  new JFrame();
        frame.setContentPane(contentPane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
      }
    });
  }
}
