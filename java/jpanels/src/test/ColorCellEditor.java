package test;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;

public class ColorCellEditor extends JPanel {
  public ColorCellEditor(Color color)
  {
    setBackground(Color.GRAY);
    setLayout(new GridBagLayout());

    // Color (with constraints)
    JComponent colorComp = new ColorComponent(color);
    colorComp.setMinimumSize(new Dimension(20, 20));
    colorComp.setPreferredSize(new Dimension(20, 20));
    colorComp.setBackground(color);
    GridBagConstraints cc = new GridBagConstraints();
    cc.insets = new Insets(0,0,0,5);
    // - add
    this.add(colorComp, cc);

    // text field (with constraints)
    ColorTextField tf = new ColorTextField(color);
    tf.setColorModel("rgb");
    GridBagConstraints tfc = new GridBagConstraints();
    tfc.weightx = 1.0;
    tfc.anchor = GridBagConstraints.LINE_START;
    // - add
    this.add(tf, tfc);

    // Advanced button (with constraints)
    JButton b = new JButton("...");
    GridBagConstraints bc = new GridBagConstraints();
    bc.insets = new Insets(0,5,0,0);
    // - add
    this.add(b, bc);
  }

}
