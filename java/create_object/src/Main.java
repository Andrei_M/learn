/**
 * See if I can create an instance of the class Object.
 *
 */
public class Main
{
  public static void main(String[] args)
  {
    System.out.println("Main.main:IN");
    System.out.println("Creating object...");
    Object aObject = new Object();
    System.out.println("Object created!");
    System.out.println("Main.main:OUT");
  }
}

