package com.andrei.main;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

import com.andrei.table.MyTableModel;

public class Main {
  JFrame myFrame;
  JTable myTable;
  TableModel myTableModel;
  private JButton myFetchButton;

  public static void main(String[] args) {
    Main main = new Main();
    main.execute(args);
  }

  private void execute(String... args)
  {
   setupFrame();
   displayFrame(myFrame);
  }

  private static void displayFrame(final JFrame theFrame) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run()
      {
        theFrame.setLocationRelativeTo(null);
        theFrame.pack();
        theFrame.setVisible(true);
      }
    });
  }

  private void setupFrame() {
    setupFetchButton();
    setupTable();

    JPanel aPanel = new JPanel();
    aPanel.setLayout(new BoxLayout(aPanel, BoxLayout.Y_AXIS));

    aPanel.add(Box.createVerticalStrut(5));
    aPanel.add(myFetchButton);
    aPanel.add(Box.createVerticalStrut(5));

    JPanel aTablePanel = new JPanel();
    BoxLayout aTablePanelLayout = new BoxLayout(aTablePanel, BoxLayout.Y_AXIS);
    aTablePanel.setLayout(aTablePanelLayout);
    aTablePanel.add(myTable.getTableHeader());
    aTablePanel.add(myTable);
    aPanel.add(aTablePanel);

    myFrame = new JFrame();
    myFrame.setContentPane(aPanel);
    myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  private void setupFetchButton() {
    myFetchButton = new JButton("Fetch Data");
    myFetchButton.setAlignmentX(Component.CENTER_ALIGNMENT);
    myFetchButton.setAlignmentY(Component.CENTER_ALIGNMENT);
    myFetchButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        for (int aRow=0; aRow<myTable.getRowCount(); aRow++) {
          for (int aColumn=0; aColumn<myTable.getColumnCount(); ++aColumn) {
            Object aCellValue = myTable.getValueAt(aRow, aColumn);
            System.out.println("Value at ("+aRow+","+aColumn+") = " + aCellValue.toString());
          }
        }
      }
    });
  }

  private void setupTable() {
    String[] columnLabels = {"Number", "X", "Y", "Z"};
    myTableModel = new MyTableModel(2, columnLabels);
    myTable = new JTable(myTableModel);

    myTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting())
          return;
        int aSelectedRow = e.getLastIndex();
        if (aSelectedRow == myTable.getModel().getRowCount()-1) {
          System.out.println("Selected the last row: " + aSelectedRow);
          TableModel aModel = myTable.getModel();
          if (aModel instanceof MyTableModel) {
            MyTableModel aTableModel = (MyTableModel) aModel;
            aTableModel.addRow();
          }
        }
      }
    });
  }
}
