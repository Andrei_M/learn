package com.andrei.table;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

public class MyTableModel extends AbstractTableModel {
  private int myRowCount;
  private String[] myColumnLabels;

  public MyTableModel(int theNumberOfRows) {
    this(theNumberOfRows, null);
  }

  public MyTableModel(int theNumberOfRows, String[] theColumnLabels) {
    myRowCount = theNumberOfRows;
    myColumnLabels = theColumnLabels;
  }

  @Override
  public int getRowCount() {
    return myRowCount;
  }

  @Override
  public int getColumnCount() {
    return 4;
  }

  @Override
  public String getColumnName(int theColumnIndex) {
    if (myColumnLabels == null) {
      return super.getColumnName(theColumnIndex);
    }

    if (theColumnIndex >= 0 && theColumnIndex < myColumnLabels.length) {
      return myColumnLabels[theColumnIndex];
    } else {
      return super.getColumnName(theColumnIndex);
    }
  }


  @Override
  public Object getValueAt(int theRowIndex, int theColumnIndex) {
    if (theColumnIndex == 0) {
      return theRowIndex;
    } else {
      return null;
    }
  }

  public void addRow() {
    addRows(1);
  }

  public void addRows(int theNumberOfRows) {
    int aFirstRow = getRowCount();
    myRowCount += theNumberOfRows;
    int aLastRow = getRowCount()-1;

    fireTableRowsInserted(aFirstRow, aLastRow);
  }

  @Override
  public Class<?> getColumnClass(int aColumnIndex)
  {
    if (aColumnIndex == 0) {
      return Integer.class;
    } else {
      return Double.class;
    }
  }
}
