package test;

import data.Color;
import data.ColorMap;

public class MainTest {
  public static void main(String[] args) {
    ColorMap aColorMap = new ColorMap();
    aColorMap.setValueLowLimit(1.3);
    aColorMap.setValueHighLimit(3.7);
    aColorMap.setValueStep(0.001);

    Color aColor = aColorMap.getColorFor(1.3);
    System.out.println("Color=" + aColor);
  }
}
