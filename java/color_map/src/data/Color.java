package data;

public class Color {
  private int myHue;
  private int mySaturation;
  private int myLuminosity;

  public Color(int theHue, int theSaturation, int theLuminosity) {
    myHue = theHue;
    mySaturation = theSaturation;
    myLuminosity = theLuminosity;
  }

  public int getMyHue() {
    return myHue;
  }
  public void setMyHue(int myHue) {
    this.myHue = myHue;
  }
  public int getMySaturation() {
    return mySaturation;
  }
  public void setMySaturation(int mySaturation) {
    this.mySaturation = mySaturation;
  }
  public int getMyLuminosity() {
    return myLuminosity;
  }
  public void setMyLuminosity(int myLuminosity) {
    this.myLuminosity = myLuminosity;
  }

  public String toString() {
    return "(hue="+myHue+"; saturation="+mySaturation+"; luminosity="+myLuminosity+")";
  }
}
