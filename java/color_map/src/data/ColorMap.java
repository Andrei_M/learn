package data;

import data.Color;

public class ColorMap {
  private double   myValueLowLimit = 0;
  private double   myValueHighLimit = 1;
  private double   myValueStep = 0.0;
  private boolean  myIsDiscrete = false;

  private int      myColorHueLowLimit = 0;    // min 0
  private int      myColorHueHighLimit = 240; // max 359
  private boolean  myReverseColorHue = true; // high to low

  private int      mySaturationLevel = 80;  // 0 - 255
  private int      myLuminosityLevel = 170;  // 0 - 255

  public void setValueLowLimit(double theValueLowLimit) {
    myValueLowLimit = theValueLowLimit;
  }

  public void setValueHighLimit(double theValueHighLimit) {
    myValueHighLimit = theValueHighLimit;
  }

  public void setValueStep(double theValueStep) {
    myValueStep = theValueStep;
    if (myValueStep < 0) {
      myValueStep = -myValueStep;
    }

    if (myValueStep > 0) {
      myIsDiscrete = true;
    } else {
      myIsDiscrete = false;
    }
  }

  public boolean isDiscrete() {
    return myIsDiscrete;
  }

  public Color getColorFor(double theValue) {
    Color aColor = null;

    double aValueRange = myValueHighLimit - myValueLowLimit;
    double aValueOffset = theValue - myValueLowLimit;
    double aValueRatio = aValueRange / aValueOffset;
    double aValuePercent = 100.0 / aValueRatio;
    aValuePercent = aValuePercent / 100;
    double aHueValue = myColorHueHighLimit * aValuePercent;

    if (myReverseColorHue) {
      aHueValue = myColorHueHighLimit - aHueValue;
    }
    aColor = new Color ((int) aHueValue, mySaturationLevel, myLuminosityLevel);

    return aColor;
  }
}
