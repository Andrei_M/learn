
class MyClass 
{
  private int objID;

  public MyClass(int objID) {
    this.objID = objID;
  }

  @Override
  protected void finalize()
  {
    System.out.println("fin:" + objID);
  }
}


public class MainTest {
  public static void main(String... args) {
    int nbrIter = 100_000;
    System.out.format("Hello %s\n", "Alligator");
    for (int i=0; i< nbrIter; i++) {
      new MyClass(i);
    }
    System.out.println("Bye bye...");
  }
}
