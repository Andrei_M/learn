#include "dialog.h"
#include <QApplication>
#include <QTime>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qsrand(QTime::currentTime().msec());

    Dialog dialog(NULL);

    dialog.setTableCount(10);
    dialog.setMaxRowCount(20);
    dialog.setMaxColumnCount(5);
    dialog.setMaxHeaderCharCount(30);
    dialog.setMaxCellCharCount(10);

    dialog.useCustomItemModel();
//    dialog.useStandardItemModel();
    dialog.createTables();

    dialog.show();

    return a.exec();
}
