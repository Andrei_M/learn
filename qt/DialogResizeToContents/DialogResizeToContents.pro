#-------------------------------------------------
#
# Project created by QtCreator 2015-04-08T17:21:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DialogResizeToContents
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    mytableview.cpp \
    utils.cpp \
    mytablemodel.cpp

HEADERS  += dialog.h \
    mytableview.h \
    utils.h \
    mytablemodel.h

FORMS    += dialog.ui
