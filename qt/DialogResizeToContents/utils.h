#ifndef UTILS_H
#define UTILS_H

#include <QString>

class QDialog;
class QTableView;

namespace Utils
{
    QString genRandomString(int stringLength = 20, bool encloseInBrackets = false);
    void centerToCurrentScreen(QDialog* diag);
    void resizeTableMinimumWidthToContents(QTableView * tableView, int widthLimit = 1000);
    void ensureMostVisibleRows(QTableView * itemView);
    void ensureMinimumVisibleRows(QTableView * itemView, int minVisibleChildren);
    int computeMinimumHeightFor(QTableView * itemView, int minVisibleChildren);
}

#endif // UTILS_H
