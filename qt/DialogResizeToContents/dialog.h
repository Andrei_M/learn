#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QList>

namespace Ui {
class Dialog;
}

class QTableView;
class MyTableView;
class QAbstractItemModel;

class Dialog : public QDialog
{
   Q_OBJECT
   typedef QDialog super;

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    void setTableCount(int tableCount) { mTableCount = tableCount; }
    void setMaxRowCount(int maxRowCount) { mMaxRowCount = maxRowCount; }
    void setMaxColumnCount(int maxColumnCount) { mMaxColumnCount = maxColumnCount; }
    void setMaxHeaderCharCount(int maxHeaderCharCount) { mMaxHeaderCharCount = maxHeaderCharCount; }
    void setMaxCellCharCount(int maxCellCharCount) { mMaxCellCharCount = maxCellCharCount; }

    void useStandardItemModel() { mUseStandardItemModel = true; }
    void useCustomItemModel() { mUseStandardItemModel = false; }

    void createTables();

protected:
   virtual void resizeEvent(QResizeEvent *);
   virtual void keyPressEvent(QKeyEvent *);
   virtual bool event(QEvent *);

private slots:
   void useCustomShortcuts();
   void createChildDialog();

private:
    void deleteCurrentTables();
    MyTableView * createTableView(int tabNumber);
    void initTableView(QTableView * tableView);
    QAbstractItemModel * createTableModel(int nbRows, int nbCols);
    void copyTablesConfigurationFrom(const Dialog * other);

private:
    Ui::Dialog *ui;
    int mDefaultTableViewWidth;
    int mDefaultTableViewHeight;
    QList<MyTableView*> mTableViews;
    bool mFirstResize;

    int mTableCount;
    int mMaxRowCount;
    int mMaxColumnCount;
    int mMaxHeaderCharCount;
    int mMaxCellCharCount;
    bool mUseStandardItemModel;
};

#endif // DIALOG_H
