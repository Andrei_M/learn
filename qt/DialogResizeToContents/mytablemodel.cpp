#include "mytablemodel.h"

MyTableModel::MyTableModel(QObject * parent)
    : super(parent)
    , mRowCount(0)
    , mColumnCount(0)
{

}

MyTableModel::MyTableModel(int nbRows, int nbColumns, QObject * parent)
    : super(parent)
    , mRowCount(0)
    , mColumnCount(0)
{
    setRowCount(nbRows);
    setColumnCount(nbColumns);
}


void MyTableModel::setRowCount(int rows)
{
    int rc = rowCount();
    if (rc == rows)
        return;
    if (rc < rows)
        insertRows(qMax(rc, 0), rows - rc, QModelIndex());
    else
        removeRows(qMax(rows, 0), rc - rows, QModelIndex());
}

void MyTableModel::setColumnCount(int columns)
{
    int cc = columnCount();
    if (cc == columns)
        return;
    if (cc < columns)
        insertColumns(qMax(cc, 0), columns - cc, QModelIndex());
    else
        removeColumns(qMax(columns, 0), cc - columns, QModelIndex());
}

bool MyTableModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);

    QVector<QVariant> emptyRow(columnCount());
    mData.insert(row, count, emptyRow);
    mVerticalHeaderData.insert(row, count, QVariant());
    mRowCount += count;
    return true;
}

bool MyTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    mData.remove(row, count);
    mVerticalHeaderData.remove(row, count);
    mRowCount -= count;
    return true;
}

bool MyTableModel::insertColumns(int column, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    for (int row=0; row<rowCount(); ++row) {
        mData[row].insert(column, count, QVariant());
    }
    mHorizontalHeaderData.insert(column, count, QVariant());
    mColumnCount += count;
    return true;
}

bool MyTableModel::removeColumns(int column, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    for (int row=0; row<rowCount(); ++row) {
        mData[row].remove(column, count);
    }
    mHorizontalHeaderData.remove(column, count);
    mColumnCount -= count;
    return true;
}

QVariant MyTableModel::data(const QModelIndex & index, int role) const
{
    if (role == Qt::DisplayRole)  {
        return mData[index.row()][index.column()];
    }
    return QVariant();
}

bool MyTableModel::setData(const QModelIndex & index, const QVariant& value, int role)
{
    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        const int row = index.row();
        const int col = index.column();
        QVector<QVariant> & rowVector = mData[row];
        rowVector.insert(col, value);
        return true;
    }
    return false;
}

QVariant MyTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            return mHorizontalHeaderData[section];
        } else if (orientation == Qt::Vertical) {
            return mVerticalHeaderData[section];
        }
    }
    return QVariant();
}

bool MyTableModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            mHorizontalHeaderData.insert(section, value);
            return true;
        } else if (orientation == Qt::Vertical) {
            mVerticalHeaderData.insert(section, value);
            return true;
        }
    }
    return false;
}
