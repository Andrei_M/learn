#include "utils.h"

#include <assert.h>

#include <QDialog>
#include <QApplication>
#include <QDesktopWidget>
#include <QTableView>
#include <QHeaderView>
#include <QScrollBar>

QString Utils::genRandomString(int stringLength, bool encloseInBrackets)
{
    int nbChars = qrand() % stringLength;
    QString str;
    str.reserve(nbChars);
//    const QString symbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ !@#$%^&*()_+=-0987654321~`\"\\][|}{';:/.,?><";
    const QString symbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (int i=0; i<nbChars; ++i)
    {
        QChar c = symbols.at(qrand() % symbols.count());
        str.push_back(c);
    }

    if (encloseInBrackets) {
        str.prepend('[');
        str.append(']');
    }

    return str;
}

void Utils::centerToCurrentScreen(QDialog* diag)
{
    QWidget * parentWidget = diag->parentWidget();
    QRect parentRect;
    if (parentWidget) {
        parentRect = parentWidget->rect();
    } else {
        QDesktopWidget * desktop = QApplication::desktop();
        int screenId = desktop->primaryScreen();
        parentRect = desktop->screenGeometry(screenId);
    }
    diag->move(parentRect.center()-diag->rect().center());
}

void Utils::resizeTableMinimumWidthToContents(QTableView * tableView, int widthLimit)
{
    tableView->resizeColumnsToContents();
    int minWidth = 0;
    minWidth += tableView->horizontalHeader()->length();
    minWidth += tableView->verticalHeader()->width();
//    if (tableView->verticalScrollBar()->isVisible()) {
//        minWidth += tableView->verticalScrollBar()->width();
//    }
    minWidth = qMin(minWidth, widthLimit);
    tableView->setMinimumWidth(minWidth);
}

void Utils::ensureMinimumVisibleRows(QTableView * itemView, int minVisibleChildren)
{
    int minListHeight = computeMinimumHeightFor(itemView, minVisibleChildren);
    itemView->setMinimumHeight(minListHeight);
}

void Utils::ensureMostVisibleRows(QTableView * itemView)
{
    int nbChildren = itemView->model()->rowCount();
    int minListHeight = computeMinimumHeightFor(itemView, nbChildren);
    itemView->setMinimumHeight(minListHeight);
}

int Utils::computeMinimumHeightFor(QTableView * itemView, int minVisibleChildren)
{
    assert(itemView || !"Invalid item view.");
    if (itemView == NULL)
    {
        return 0;
    }

    assert(itemView->model() || !"Item view has no model.");
    if ( ! itemView->model())
    {
        return 0;
    }

    // header height
    int headerHeight = itemView->horizontalHeader()->size().height();
    int nbItems = itemView->model()->rowCount();
    if (nbItems < 1)
    {
        return headerHeight;
    }

    // items height
    int itemHeight = itemView->verticalHeader()->sectionSize(0);
    int itemsHeight = qMin(nbItems, minVisibleChildren) * itemHeight;

    // horizontal scroll bar
    int hScrollBarHeight = 0;
    //if (itemView->horizontalScrollBar()->isVisible())
    {
        // commented out. For some reason, the scroll bar is always reported as visible
        // which can introduce unwanted space.
        //hScrollBarHeight = itemView->horizontalScrollBar()->height();
    }

    // total height
    int listHeight = itemsHeight + headerHeight + hScrollBarHeight + 2; // add 2 extra pixels to avoid the vScrollBar
    return listHeight;
}
