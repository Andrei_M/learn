#include "mytableview.h"

#include <QApplication>
#include <QFont>
#include <QWheelEvent>
#include <QHeaderView>
#include <QScrollBar>
#include <QDebug>

MyTableView::MyTableView(QWidget *parent) :
    super(parent)
{
    connect(verticalHeader(), SIGNAL(sectionPressed(int)),
            this, SLOT(verticalHeaderSectionPressed(int)));
    connect(verticalHeader(), SIGNAL(sectionEntered(int)),
            this, SLOT(verticalHeaderSectionEntered(int)));
}

MyTableView::~MyTableView()
{
    qDebug() << "MyTableView::~MyTableView()";
}

void MyTableView::wheelEvent(QWheelEvent *event)
{
    if (event->modifiers() & Qt::CTRL) {
        int numDegrees = event->delta() / 8;
        int numSteps = numDegrees / 15;
        QFont appFont = qApp->font();

        int oldFontSize = appFont.pointSize();
        int fontSize =  oldFontSize + numSteps;
        if (fontSize > 0)
        {
            appFont.setPointSize(fontSize);
            qApp->setFont(appFont);
            event->accept();
        }
    } else {
        super::wheelEvent(event);
    }
}

void MyTableView::keyPressEvent(QKeyEvent *event)
{
    if (mUseCustomShortcuts) {
        if (event->modifiers() & Qt::ShiftModifier) {
            switch(event->key()) {
            case Qt::Key_Up:
            case Qt::Key_Down:
            case Qt::Key_Left:
            case Qt::Key_Right:
            {
                if (selectionModel()->selection().count() == 1)
                {
                    QItemSelection sel = selectionModel()->selection();
                    QModelIndex br = sel.first().bottomRight();
                    selectionModel()->setCurrentIndex(br, QItemSelectionModel::NoUpdate);
                }
            }
            }
        }
    }

    super::keyPressEvent(event);
}

void MyTableView::verticalHeaderSectionPressed(int section)
{
    qDebug() << "Pressed section " << section;
    QItemSelection sel = selectionModel()->selection();
    QModelIndex br = sel.first().bottomRight();
    selectionModel()->setCurrentIndex(br, QItemSelectionModel::NoUpdate);
}

void MyTableView::verticalHeaderSectionEntered(int section)
{
    static int prevSection = -1;
    if (prevSection == -1) {
        prevSection = section;
    }

    QItemSelection sel = selectionModel()->selection();
    QModelIndex currentCellTarget;

    if (section > prevSection)
    {
        currentCellTarget = sel.first().bottomRight();
    } else {
        int top = sel.first().top();
        int right = sel.first().right();
        currentCellTarget = model()->index(top, right);
    }

    selectionModel()->setCurrentIndex(currentCellTarget, QItemSelectionModel::NoUpdate);
    qDebug() << "Entered section " << section;

    prevSection = section;
}

void MyTableView::mousePressEvent(QMouseEvent *event)
{
    super::mousePressEvent(event);
}

void MyTableView::mouseMoveEvent(QMouseEvent *event)
{
    super::mouseMoveEvent(event);
}
