#ifndef MYTABLEVIEW_H
#define MYTABLEVIEW_H

#include <QTableView>

class MyTableView : public QTableView
{
    Q_OBJECT
    typedef QTableView super;

public:
    explicit MyTableView(QWidget *parent = 0);
    void useCustomShortcuts(bool flag) { mUseCustomShortcuts = flag; }
    ~MyTableView();


protected:
    virtual void wheelEvent(QWheelEvent *event);
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);

signals:

private slots:
    void verticalHeaderSectionPressed(int section);
    void verticalHeaderSectionEntered(int section);

private:
    bool mUseCustomShortcuts;

};

#endif // MYTABLEVIEW_H
