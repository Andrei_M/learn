#include "dialog.h"
#include "ui_dialog.h"

#include <QStandardItemModel>
#include <QDebug>
#include <QResizeEvent>

#include "mytableview.h"
#include "mytablemodel.h"
#include "utils.h"

Dialog::Dialog(QWidget *parent)
:
        QDialog(parent, Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint)
      , ui(new Ui::Dialog)
      , mFirstResize(true)
      , mTableCount(2)
      , mMaxRowCount(10)
      , mMaxColumnCount(5)
      , mMaxHeaderCharCount(30)
      , mMaxCellCharCount(10)
      , mUseStandardItemModel(true)
{
    static int dialogNumber = 0;
    ++dialogNumber;
    setWindowTitle("Dialog #" + QString::number(dialogNumber));
    ui->setupUi(this);

    connect(ui->mUseCustomShortcutsCheckBox, SIGNAL(stateChanged(int)),
            this, SLOT(useCustomShortcuts()));
    connect(ui->mCreateChildDialog, SIGNAL(clicked()),
            this, SLOT(createChildDialog()));
    useCustomShortcuts();

    ui->radioButton->setText(Utils::genRandomString(30));
    ui->pushButton->setText(Utils::genRandomString(30));
    ui->pushButton_2->setText(Utils::genRandomString(30));

    mDefaultTableViewWidth = QTableView().minimumWidth();
    mDefaultTableViewHeight = QTableView().minimumHeight();

    resize(0, 0);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::createTables()
{
    deleteCurrentTables();

    for (int i=0; i<mTableCount; ++i) {
        mTableViews.append(createTableView(i+1));
        initTableView(mTableViews.last());
    }
}

void Dialog::deleteCurrentTables()
{
    for (int tabIndex=0; tabIndex < ui->tabWidget->count(); ++tabIndex)
    {
        delete ui->tabWidget->widget(tabIndex);
    }
    ui->tabWidget->clear();
    mTableViews.clear();
}

MyTableView * Dialog::createTableView(int tabNumber)
{
    QWidget * tab = new QWidget();
    tab->setObjectName("Table " + QString::number(tabNumber));
    QGridLayout * gridLayout = new QGridLayout(tab);
    gridLayout->setSpacing(0);
    gridLayout->setObjectName("gridLayout_" + QString::number(tabNumber));
    gridLayout->setContentsMargins(0, 0, 0, 0);

    MyTableView * tableView = new MyTableView(tab);
    tableView->setObjectName("tableView_" + QString::number(tabNumber));
    tableView->setFrameShape(QFrame::NoFrame);
    gridLayout->addWidget(tableView, 0, 0, 1, 1);

    ui->tabWidget->addTab(tab, tab->objectName());

    return tableView;
}


void Dialog::initTableView(QTableView * tableView)
{
    tableView->setModel(
            createTableModel(qMax(1,qrand()%mMaxRowCount+1),
                             qMax(1,qrand()%mMaxColumnCount+1)));

    tableView->setSortingEnabled(false);
    int penultimateCol = tableView->model()->columnCount() > 1
            ? tableView->model()->columnCount() - 2
            : -1;
    bool enablePenultimateColStretch = false;
#if QT_VERSION >= 0x050000
    tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
    if (enablePenultimateColStretch && penultimateCol != -1) {
        tableView->horizontalHeader()->setSectionResizeMode(penultimateCol, QHeaderView::Stretch);
    }
#else
    tableView->horizontalHeader()->setResizeMode(QHeaderView::Interactive);
    if (enablePenultimateColStretch && penultimateCol != -1) {
        tableView->horizontalHeader()->setResizeMode(penultimateCol, QHeaderView::Stretch);
    }
#endif
//    tableView->horizontalHeader()->setStretchLastSection(true);
    tableView->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);

    Utils::resizeTableMinimumWidthToContents(tableView);
    Utils::ensureMostVisibleRows(tableView);
}

QAbstractItemModel * Dialog::createTableModel(int nbRows, int nbColumns)
{
    QAbstractItemModel * tableModel;
    if (mUseStandardItemModel) {
        tableModel = new QStandardItemModel(nbRows, nbColumns, this);
    } else {
        tableModel = new MyTableModel(nbRows, nbColumns, this);
    }

    for (int col=0; col < tableModel->columnCount(QModelIndex()); ++col) {
        tableModel->setHeaderData(col, Qt::Horizontal, Utils::genRandomString(mMaxHeaderCharCount, true), Qt::DisplayRole);
    }

    for (int row=0; row < tableModel->rowCount(QModelIndex()); ++row) {
        for (int col=0; col < tableModel->columnCount(QModelIndex()); ++col) {
            tableModel->setData(tableModel->index(row, col), Utils::genRandomString(mMaxCellCharCount), Qt::DisplayRole);
        }
    }

    return tableModel;
}

void Dialog::useCustomShortcuts()
{
    foreach (MyTableView * tableView, mTableViews) {
        tableView->useCustomShortcuts(ui->mUseCustomShortcutsCheckBox->isChecked());
    }
}

void Dialog::createChildDialog()
{
    Dialog * dialog = new Dialog(this);
    dialog->copyTablesConfigurationFrom(this);
    dialog->createTables();
    dialog->show();
}

void Dialog::copyTablesConfigurationFrom(const Dialog * other)
{
    this->setTableCount(other->mTableCount);
    this->setMaxRowCount(other->mMaxRowCount);
    this->setMaxColumnCount(other->mMaxCellCharCount);
    this->setMaxHeaderCharCount(other->mMaxHeaderCharCount);
    this->setMaxCellCharCount(other->mMaxCellCharCount);
    this->mUseStandardItemModel = other->mUseStandardItemModel;
}

void Dialog::resizeEvent(QResizeEvent * event)
{
    // restore table minimum width
    if (mFirstResize) {
        foreach (QTableView * tableView, mTableViews) {
            Utils::resizeTableMinimumWidthToContents(tableView, mDefaultTableViewWidth);
            Utils::ensureMinimumVisibleRows(tableView, 1);
        }
        mFirstResize = false;
    }

    super::resizeEvent(event);
}

void Dialog::keyPressEvent(QKeyEvent *event)
{
    if (event->modifiers() & Qt::AltModifier)
    {
        if (event->key() == Qt::Key_K)
        {
            event->accept();
            ui->mUseCustomShortcutsCheckBox->toggle();
        }
        if (event->key() == Qt::Key_M)
        {
            event->accept();
            createChildDialog();
        }
    }
}

bool Dialog::event(QEvent *event)
{
    return super::event(event);
}
