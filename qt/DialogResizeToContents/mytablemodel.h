#ifndef MYTABLEMODEL_H
#define MYTABLEMODEL_H

#include <QAbstractTableModel>
#include <QVector>
#include <QVariant>

class MyTableModel : public QAbstractTableModel
{
    Q_OBJECT
    typedef QAbstractTableModel super;

public:
    MyTableModel(QObject * parent = NULL);
    MyTableModel(int nbRows, int nbColumns, QObject * parent = NULL);
    void setRowCount(int rows);
    void setColumnCount(int columns);

    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const
    { Q_UNUSED(parent); return mRowCount; }
    virtual int columnCount(const QModelIndex & parent = QModelIndex()) const
    { Q_UNUSED(parent); return mColumnCount; }
    int lastRow() const { return rowCount() - 1; }
    int lastColumn() const { return columnCount() - 1; }
    int firstRow() const { return 0; }
    int firstColumn() const { return 0; }
    bool hasRows() const { return rowCount() > 0; }

    virtual QVariant data(const QModelIndex & index, int role) const;
    virtual bool setData(const QModelIndex & index, const QVariant& value, int role = Qt::EditRole);

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    virtual bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role);

    virtual bool insertRows(int row, int count, const QModelIndex &parent);
    virtual bool removeRows(int row, int count, const QModelIndex &parent);
    virtual bool insertColumns(int column, int count, const QModelIndex &parent);
    virtual bool removeColumns(int column, int count, const QModelIndex &parent);


private:
    int mRowCount;
    int mColumnCount;
    QVector<QVector<QVariant>> mData;
    QVector<QVariant> mHorizontalHeaderData;
    QVector<QVariant> mVerticalHeaderData;
};

#endif // MYTABLEMODEL_H
