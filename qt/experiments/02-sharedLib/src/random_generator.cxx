#include "random_generator.h"
#include <stdlib.h>
#include <time.h>

RandomGenerator::RandomGenerator()
{
  srand(time(0));
}

int RandomGenerator::nextInt() const
{
  return rand();
}

int RandomGenerator::nextInt(int max) const
{
  return nextInt() % max;
}
