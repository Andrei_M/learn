#ifndef RANDOM_GENERATOR_H
#define RANDOM_GENERATOR_H

class RandomGenerator
{
public:
  RandomGenerator();
  int nextInt() const;
  int nextInt(int max) const;
};
#endif
