#include <QApplication>
#include <QLabel>
#include "Dumb.h"

int main (int argc, char *argv[])
{
  QApplication a(argc, argv);
  int x = dumbFunction(5);
  QString message = QString("Hello, Qtourian. Your ID is: %1").arg(x);
  QLabel label(message);
  label.show();
  qDebug("You should not see this! Unless you consoled your app at some point in the .pro file");

  return a.exec();
}
