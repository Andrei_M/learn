import QtQuick 1.1

Item {
   id: container
   width: 100
   height: 120
   property alias toolImage: image1
   signal clicked()

   Image {
      property alias t
      id: image1
      width: 100
      height: 100
      fillMode: Image.PreserveAspectFit
      source: "qrc:/qtquickplugin/images/template_image.png"

      MouseArea {
         hoverEnabled: true
         anchors.fill: parent
         onClicked: container.clicked()
      }
   }
}
