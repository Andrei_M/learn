import QtQuick 1.1

Rectangle {
   id: container
   property alias text: label.text
   property alias icon: buttonImage.source

   property color buttonColor: "lightblue"
   property color onHoverBorderColor: "gold"
   property color borderColor: "white"

   signal clicked()

   border.color: borderColor
   border.width: 3
   color: mouseArea.pressed ? Qt.darker(buttonColor, 1.5) : buttonColor

   Image {
      id: buttonImage
      fillMode: Image.PreserveAspectFit
      anchors.verticalCenter: container.verticalCenter
      anchors.left: container.left
      anchors.leftMargin: 5
   }

   Text {
      id: label
      text: "Undefined"
      font.pointSize: 10
      transformOrigin: Item.Center

      anchors.verticalCenter: container.verticalCenter
      anchors.left: buttonImage.right
      anchors.leftMargin: 5

      verticalAlignment: Text.AlignBottom

      wrapMode: Text.Wrap
      width: container.width - buttonImage.width
   }

   MouseArea {
      id: mouseArea
      hoverEnabled: true
      anchors.fill: parent

      onClicked:
      {
         container.clicked()
      }
      onEntered: container.border.color = onHoverBorderColor
      onExited: container.border.color = borderColor
   }
}
