import QtQuick 1.1

Rectangle {
   id: page
   width: 500; height: 800
   color: "lightgray"
   property alias title: titleText.text

   Flow
   {
      id: flow1
      anchors.right: parent.right
      anchors.rightMargin: 8
      anchors.left: parent.left
      anchors.leftMargin: 8
      anchors.top: parent.top
      anchors.topMargin: 12
      spacing: 8

      Button {
         width: 120
         height: 48
         text: qsTr("Add AME environment")
         icon: "qrc:/qml/images/box_32.png"
      }
      Button {
         width: 120
         height: 48
         text: qsTr("Add tool")
         icon: "qrc:/qml/images/tool_32.png"
      }
   }

   Text {
      id: titleText
      text: qsTr("No AME Environment set")
      wrapMode: Text.WordWrap
      anchors.centerIn: parent
      font.pointSize: 24; font.bold: true

      MouseArea { id: mouseArea; anchors.fill: parent }

      states: State {
         name: "down"; when: mouseArea.pressed == true
         PropertyChanges { target: titleText; rotation: 360; color: "red" }
      }

      transitions: [
         Transition {
            from: ""; to: "down";
            ParallelAnimation {
               NumberAnimation { properties: "rotation"; duration: 500; easing.type: Easing.InOutQuad }
               ColorAnimation { easing.type: Easing.InOutBack; duration: 500 }
            }
         },

         Transition {
            from: "down"; to: "";
            SequentialAnimation {
               NumberAnimation { properties: "rotation"; duration: 250; easing.type: Easing.InOutQuad }
               ColorAnimation { easing.type: Easing.InOutQuad; duration: 250 }
            }
         }
      ]

   }
}
