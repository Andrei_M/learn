import QtQuick 1.1

Row {
   anchors.centerIn: parent
   spacing: parent.width/6

   Button {
      width: 120
      height: 48
      text: qsTr("Add AME environment")
      icon: "qrc:/qml/images/box_32.png"
   }

   Button {
      width: 120
      height: 48
      text: qsTr("Add tool")
      icon: "qrc:/qml/images/tool_32.png"
   }
}
