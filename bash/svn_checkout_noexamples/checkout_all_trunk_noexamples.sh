#!/bin/bash

# svn checkout all TRUNK
#DEST_DIR=$1
DEST_DIR=/home/test/FCOMP/SALL/test-noex
ALL_TRUNK_URL=http://subversion.samtech.com/subversion/samtech/components/ALL/TRUNK
if [ -d $DEST_DIR ]
  then echo "$DEST_DIR already created"
  else svn co --depth=immediates $ALL_TRUNK_URL $DEST_DIR
fi

# update all directories except 'examples'
for i in $DEST_DIR/*
do 
  if [ $i == "$DEST_DIR/examples" ]
    then continue
  fi
 
  # print current filename
  echo $i
  
  # update ...
  svn up $i --depth=infinity
done
