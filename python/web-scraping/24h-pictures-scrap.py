import bs4
import os
import requests
from selenium import webdriver
import re
import time
from urllib import request
import shutil


root_url = 'http://tempsreel.nouvelobs.com'
index_url = root_url + '/galeries-photos/photo/20120206.OBS0683/24-heures-en-images.html'

def get_images_url_and_desc(nb_images):
    browser = webdriver.Firefox()

    browser.get(index_url)
    images = []
    for i in range(nb_images):
        assert '24 heures' in browser.title
        image_url_tag = browser.find_element_by_css_selector('#obs-img')  # Find the image holder
        image_url = image_url_tag.get_attribute('src')
        image_desc_tag = browser.find_element_by_css_selector('#obs-desc > p:nth-child(1)')
        image_desc = image_desc_tag.text
        images.append({"url": image_url, "desc": image_desc})
        next_button = browser.find_element_by_css_selector('#obs-tpl-gal > section.top-gal > article > div.col-art-gal > aside > div > a:nth-child(2)')
        next_button.click()
        # time.sleep(0.1)

    browser.close()

    return images

def get_nb_images():
    response = requests.get(index_url)
    soup = bs4.BeautifulSoup(response.text)
    p_tag = soup.select('#obs-tpl-gal > section.top-gal > article > div.col-art-gal > aside > p')
    p_content =  p_tag[0].contents[1]
    nb_images = re.search(r'([0-9]+)$',p_content).group(1)
    return int(nb_images)

def create_download_directory():
    parent_dir = os.path.dirname(os.path.realpath(__file__))
    dest_dir =  parent_dir
    dest_dir += os.sep
    dest_dir += 'pictures'
    dest_dir += os.sep
    if (os.path.exists(dest_dir)):
        now = time.strftime("%Y%m%d_%H%M%S")
        parent_dir = os.path.dirname(dest_dir)
        backup_dir = parent_dir + '.backup.' + str(now)
        shutil.move(dest_dir, backup_dir)
    os.makedirs(dest_dir)
    return dest_dir

def save_image_to_file(url, filePath):
    print("Saving image {0} to file {1}", url, filePath)
    request.urlretrieve(url, filePath)

def save_desc_to_file(content, filePath):
    text_file = open(filePath, "w")
    text_file.write(content)
    text_file.close()

def fetch_images():
    dest_dir = create_download_directory()
    nb_images = get_nb_images()
    print("Number of images to download: " + str(nb_images))
    assert(nb_images>0)

    steps = nb_images
    images_info = get_images_url_and_desc(steps)
    current_image_index = 1
    for image_info in images_info:
        baseFileName = str(dest_dir) + os.sep + str(current_image_index).zfill(2)
        save_image_to_file(image_info['url'], baseFileName + '.jpg')
        save_desc_to_file(image_info['desc'], baseFileName + '.txt')
        current_image_index+=1

if __name__ == '__main__':
    fetch_images()
