from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import time

browser = webdriver.Firefox()
browser.get("http://www.yahoo.com") # Load page
assert "Yahoo" in browser.title
elem = browser.find_element_by_name("p") # Find the query box
elem.send_keys("seleniumhq" + Keys.RETURN)
time.sleep(0.2) # Let the page load, will be added to the API
try:
    link_elem = browser.find_element_by_xpath("//a[contains(@href,'http://docs.seleniumhq.org')]")
    link_elem.click()
except NoSuchElementException:
    assert 0, "can't find seleniumqh"
while True:
    time.sleep(0.1)
# browser.close()