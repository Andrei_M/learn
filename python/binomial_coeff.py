#!/bin/python

# compute the binomial coeffeicient, after the formula
#      n         n!
#    (   ) = -----------
#      m      m! (n-m)!
# where m and n are integers bound by 0<=m<=n

import argparse
from math import factorial as fact
import matplotlib.pyplot as plt

def binomialCoeff(n, m):
  return int(fact(n) / (fact(m) * fact(n-m)))

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('n')
  # parser.add_argument('m')
  args = parser.parse_args()
  n = int(args.n)
  # m = int(args.m)
  results = []
  for i in range(1,n+1):
    results.append(binomialCoeff(n,i))

  # result = binomialCoeff(n, m)
  indexes = range(0,len(results))
  print(results)
  print(max(results))
  plt(indexes, results, 'ro')
  plt.show()
  

if __name__ == "__main__":
  main()
