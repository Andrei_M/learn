/*
 * main_test.cxx
 *
 *  Created on: May 3, 2012
 *      Author: test
 */

#include <iostream>
using namespace std;

class A{
public:
  A(){cout<<"A::A"<<endl;}
  A(const A& other) {cout<<"A::A(const&A)"<<endl;}
};
class B : public A {
public:
  B(){cout<<"B::B"<<endl;}
  B(const B& other) {cout<<"B::B(const&B)"<<endl;}
};

class C{
public:
  virtual A* foo()
  {
    cout << "C::foo" << endl;
    A *a = new A();
    return a;
  }
  virtual ~C(){}
};

class D : public C{
public:
  virtual B* foo()
  {
    cout << "D::foo" << endl;
    B *b = new B();
    return b;
  }
  virtual ~D(){}
};


int main(void)
{
  C c;
  C *d = new D();

  A* a = c.foo();
  A* b = d->foo();

  delete a;
  delete b;
  delete d;
}
