/*
 * test.cxx
 *
 *  Created on: May 23, 2012
 *      Author: test
 */
#include <iostream>
#include "MyClass.hxx"

int main(const int argc, const char* argv[])
{
  using namespace std;
  cout << "Hello man!" << endl;

  MyClass mc(5);
  cout << mc.GetIntValue();
  return 0;
}


