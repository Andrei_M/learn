/*
 * MyClass.hxx
 *
 *  Created on: May 23, 2012
 *      Author: test
 */

#ifndef MYCLASS_HXX_
#define MYCLASS_HXX_

#include <ostream>

class MyClass
{
public:
  MyClass(int);

  int GetIntValue() const;

private:
  int myIntValue;
};


#endif /* MYCLASS_HXX_ */
