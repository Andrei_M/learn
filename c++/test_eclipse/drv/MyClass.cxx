/*
 * MyClass.cxx
 *
 *  Created on: May 23, 2012
 *      Author: test
 */

#include "MyClass.hxx"
#include <iostream>

MyClass::MyClass(int theIntValue)
{
  myIntValue = theIntValue;
}

int MyClass::GetIntValue() const
{
  return myIntValue;
}
