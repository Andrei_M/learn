/*
 * MyClass.cpp
 *
 *  Created on: Jun 14, 2012
 *      Author: test
 */

#include "MyClass.h"
#include <iostream>
using namespace std;

int MyClass::_InstanceCount = 0;

MyClass::MyClass()
{
  _InstanceCount += 1;

  if (_InstanceCount < 10) {
    _Child = new MyClass();
  }
}

MyClass::MyClass(const MyClass& other)
{
  cout << "MyClass::Copy_Constructor:IN" << endl;
  _InstanceCount += 1;

  if (_Child != 0) {
    delete _Child;
    _Child = 0;
  }

  if (other._Child != 0) {
    _Child = new MyClass(*(other._Child));
  }

  cout << "MyClass::Copy_Constructor:OUT" << endl;
}

MyClass& MyClass::operator=(const MyClass& rhs)
{
  cout << "MyClass::operator=:IN" << endl;
  if (this == &rhs) {
    cout << "MyClass::operator=:OUT" << endl;
    return *this;
  }

  if (_Child != 0) {
    delete _Child;
    _Child = 0;
  }

  if (rhs._Child != 0) {
    _Child = new MyClass(*(rhs._Child));
  }

  cout << "MyClass::operator=:OUT" << endl;
  return *this;
}
