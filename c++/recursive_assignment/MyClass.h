/*
 * MyClass.h
 *
 *  Created on: Jun 14, 2012
 *      Author: test
 */

#ifndef MYCLASS_H_
#define MYCLASS_H_

class MyClass
{
public:
  MyClass();
  MyClass& operator = (const MyClass& rhs);
  explicit MyClass(const MyClass& other);

private:
  MyClass* _Child;
  static int _InstanceCount;
};


#endif /* MYCLASS_H_ */
