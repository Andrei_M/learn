/*
 * MainTest.cxx
 *
 *  Created on: Jun 14, 2012
 *      Author: test
 */

#include <iostream>
#include "MyClass.h"
using namespace std;

int main(void)
{
  cout << "MainTest.main:IN" << endl;
  MyClass a;
  MyClass b;
  a = b;
  cout << "MainTest.main:OUT" << endl;
  return 0;
}


