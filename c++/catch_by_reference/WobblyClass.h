/*
 * WobblyClass.h
 *
 *  Created on: Nov 14, 2012
 *      Author: test
 */

#ifndef WOBBLYCLASS_H_
#define WOBBLYCLASS_H_

namespace domain
{

class WobblyClass
{
public:
  WobblyClass();
  void ProblematicMethod(); // throws MyException
};

} /* namespace domain */
#endif /* WOBBLYCLASS_H_ */
