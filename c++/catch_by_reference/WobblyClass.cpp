/*
 * WobblyClass.cpp
 *
 *  Created on: Nov 14, 2012
 *      Author: test
 */

#include "WobblyClass.h"
#include "MyException.h"
#include <iostream>
using namespace std;

namespace domain
{

WobblyClass::WobblyClass()
{
}

void WobblyClass::ProblematicMethod()
{
  cout << "WobblyClass::ProblematicMethod():throwing MyException..." << endl;
  throw MyException();
}

} /* namespace domain */
