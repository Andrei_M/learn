/*
 * Main.cxx
 *
 *  Put the different catching exception ways to the test:
 *     1. Catch by object
 *     2. Catch by reference
 *     3. Catch by const-reference
 *
 * In the code below, it is seen (by object Id), that exceptions caught by reference
 * are not duplicated, while catching by copy makes a... copy of the exception object.
 *
 *  Created on: Nov 14, 2012
 *      Author: test
 */

#include "WobblyClass.h"
#include "MyException.h"

using namespace domain;

#include <iostream>
using namespace std;

int main (int argc, char* argv[])
{
  WobblyClass wc;

  cout << " --- Case 1: Catch by copy" << endl;
  try {
    wc.ProblematicMethod();
  } catch (MyException e) {
    cout << "Exception id = " << e.Id() << endl;
  }
  cout << "--------------------------" << endl;

  cout << " --- Case 2: Catch by reference" << endl;
  try {
    wc.ProblematicMethod();
  } catch (MyException& e) {
    cout << "Exception id = " << e.Id() << endl;
  }
  cout << "--------------------------" << endl;

  cout << " --- Case 3: Catch by const-reference" << endl;
  try {
    wc.ProblematicMethod();
  } catch (const MyException& e) {
    cout << "Exception id = " << e.Id() << endl;
  }
  cout << "--------------------------" << endl;

  return 0;
}


