/*
 * MyException.cpp
 *
 *  Created on: Nov 14, 2012
 *      Author: test
 */

#include "MyException.h"
#include <iostream>
using namespace std;

namespace domain
{

MyException::MyException()
:
    _Id(1)
{
  cout << "MyException::MyException()" << endl;;
}

MyException::MyException(const MyException& other)
:
    _Id(other.Id() + 1)
{
  cout << "MyException::MyException(const MyException& other)" << endl;
}

int MyException::Id() const
{
  return _Id;
}

} /* namespace domain */
