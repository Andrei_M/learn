/*
 * MyException.h
 *
 *  Created on: Nov 14, 2012
 *      Author: test
 */

#ifndef MYEXCEPTION_H_
#define MYEXCEPTION_H_

namespace domain
{

class MyException
{
public:
  MyException();
  MyException(const MyException& other); // copy-constructor
  int Id() const;

private:
  int _Id;
};

} /* namespace domain */
#endif /* MYEXCEPTION_H_ */
