class OriginalClass 
{
public:
    OriginalClass(int iValue);

    int GetPrivateMember();

    /* No way to set the private member after construction time */

private:
    int _PrivateMember;
};
