#include "original_class.hxx"
#include "fake_class.hxx"

#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{
    cout << "Hello" << endl;

    OriginalClass aOriginalClass(5);

    cout << "Displaying the private member accessed through it's delegated getter: " 
        << aOriginalClass.GetPrivateMember() << endl;

    FakeClass* aFakeClass = reinterpret_cast<FakeClass*>(&aOriginalClass);
    aFakeClass->PublicMember = -1;
    
    cout << "Displaying the private member accessed through it's delegated getter: " 
        << aOriginalClass.GetPrivateMember() << endl;

    return 0;
}
