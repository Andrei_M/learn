/*
 * MainTest.cpp
 *
 * See the difference in the way C/C++ handles arrays and pointers.
 *
 *  Created on: Jun 20, 2012
 *      Author: test
 */

#include <iostream>
using namespace std;

class CharOp {
public:
  void foo (char []);
  void bar (const char * );

private:
  char _Chars[];
};

int main(int argc, char *argv[])
{
  char chars[]  = "Bang! char array!\0";
  char *charPtr = "Gang! char pointer!\0";

  CharOp op;
  op.foo (chars);
  op.foo (charPtr);
  op.bar (chars);
  op.bar (charPtr);
  return 0;
}

void CharOp::foo(char chars[])
{
  cout << "foo.IN" << endl;
  cout << "foo:char[]=" << chars << endl;
  cout << "foo.OUT" << endl;
}

void CharOp::bar(const char *charPtr)
{
  cout << "bar.IN" << endl;
  cout << "bar:char*=" << charPtr << endl;
  cout << "bar.OUT" << endl;
}
