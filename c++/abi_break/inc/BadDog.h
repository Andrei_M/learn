#ifndef BAD_DOG_H
#define BAD_DOG_H

#include <iostream>

class BadDog
{
public:
  BadDog() 
    : 
    _breedId(1), 
    _name("Animalously")
  {}
  bool setName(std::string newName); 
  inline std::string name() const { return _name; }
  bool setBreedId(int newBreedId);
  inline int breedId() const { return _breedId; }

private:
  int _breedId;
  std::string _name;
};

#endif // BAD_DOG_H
