#include "BadDog.h"

bool BadDog::setName(std::string newName)
{

  if (newName.empty() || !std::isupper(newName[0])) {
    std::cout << "Wanna name your dog like that? \"" << newName << "\"? Really?" << std::endl;
    std::cout <<  "!Show the poor dog some respect, and choose a proper Name for Great sake!" << std::endl;
    return false;
  }
  
  _name = newName;
  return true;
}
    
bool BadDog::setBreedId(int newBreedId)
{
  if (newBreedId < 0 || newBreedId > 1024) {
    return false;
  }
  
  _breedId = newBreedId;
  return true;
}
