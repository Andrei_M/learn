#include <iostream>
#include "BadDog.h"

int main()
{
  BadDog dog;
  std::string dogName = dog.name();
  std::cout << "Calling dog: " << dogName << "..." << std::endl;

  dog.setName("billy-boy");
  dog.setName("Chow-how");
  dogName = dog.name();
  std::cout << "Calling dog: " << dogName << "..." << std::endl;

  std::cout << "End of show. Get out of here now!" << std::endl;
  return 0;
}

