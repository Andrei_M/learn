class Command
{
public:
    virtual int Execute() = 0;

    virtual ~Command();

protected:
    Command();
}
