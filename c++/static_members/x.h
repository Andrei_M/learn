#ifndef X_H 
#define X_H 

class X 
{
public:
  X() : a(0),b(0.0),c(0) { /* empty body */ }

private:
  int a;
  double b;
  int *c;
};
#endif // X_H
