#include <iostream>
#include "x.h"
#include "y.h"
#include "z.h"

using std::cout;
using std::endl;

int main(void)
{
  cout << "sizeof(X) = " << sizeof(X) << endl;
  cout << "sizeof(Y) = " << sizeof(Y) << endl;
  cout << "sizeof(Z) = " << sizeof(Z) << endl;

  return 0;
}
