/*
 * MyClass.h
 *
 *  Created on: Oct 1, 2012
 *      Author: test
 */

#ifndef MYCLASS_H_
#define MYCLASS_H_

class MyClass
{
private:
  int _x;

public:
  MyClass(int x);
  ~MyClass();
  void SetX(int x);
  MyClass& operator=(const MyClass& rhs);
  void Delete() const;
  void Dump() const;
};

#endif /* MYCLASS_H_ */
