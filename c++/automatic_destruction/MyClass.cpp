/*
 * MyClass.cpp
 *
 *  Created on: Oct 1, 2012
 *      Author: test
 */

#include "MyClass.h"
#include <iostream>
using namespace std;
MyClass::MyClass(int x)
:
    _x(x)
{
  cout << "MyClass::MyClass()" << endl;
}

MyClass::~MyClass()
{
  cout << "MyClass::~MyClass()" << endl;
}

void MyClass::SetX(int x)
{
  _x = x;
}

MyClass& MyClass::operator =(const MyClass& rhs)
{
  if (this==&rhs) {
    return *this;
  }

  // some things happen here

  // return this
  return *this;
}

void MyClass::Delete() const
{
  delete(this);
}

void MyClass::Dump() const
{
  cout << "x=" << _x << endl;
}
