/*
 * Main.cxx
 *
 *  Created on: Oct 1, 2012
 *      Author: test
 */

#include "MyClass.h"

int main()
{
  {
    MyClass *mc = new MyClass(-8);
    mc->Dump();
    mc->Delete();
    mc = 0;
    mc->Dump();
    mc = new MyClass(5);
    mc->Dump();
  }

  return 0;
}

