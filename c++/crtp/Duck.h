/*
 * Duck.h
 *
 *  Created on: Jun 22, 2012
 *      Author: test
 */

#include "Base.h"

#ifndef DUCK_H_
#define DUCK_H_

class Duck : public Base<Duck>
{
public:
  Duck();
  void GreetFromDerived() const;
};

#endif /* DUCK_H_ */
