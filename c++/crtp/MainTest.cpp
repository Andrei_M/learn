/*
 * MainTest.cpp
 *
 * Play and test the Curiously Recurring Template Pattern (CRTP)
 * see
 * http://stackoverflow.com/questions/4173254/what-is-the-curiously-recurring-template-pattern-crtp/4173298#4173298
 *  Created on: Jun 22, 2012
 *      Author: test
 */

#include <iostream>
#include "Dog.h"
#include "Duck.h"

using namespace std;

template <typename T>
void SayHello(const Base<T>& instance)
{
  instance.SayHello();
}

int main(void)
{
  cout << "main:IN" << endl;

  Dog dog;
  SayHello(dog);

  Duck duck;
  SayHello(duck);

  cout << "main:OUT" << endl;
  return 0;
}


