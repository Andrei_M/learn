/*
 * Base.h
 *
 *  Created on: Jun 22, 2012
 *      Author: test
 */

#ifndef BASE_H_
#define BASE_H_

template <typename T>
class Base
{
public:
  Base():myChild(0){};
  void SayHello() const
  {
    static_cast<const T*>(this)->GreetFromDerived();
  }

private:
  T* myChild; // not used for the moment, just to show you can have a pointer
  // to yourself here
};

#endif /* BASE_H_ */
