/*
 * Dog.h
 *
 *  Created on: Jun 22, 2012
 *      Author: test
 */

#include "Base.h"

#ifndef DOG_H_
#define DOG_H_

class Dog : public Base<Dog>
{
public:
  Dog();
  void GreetFromDerived() const;
};

#endif /* DOG_H_ */
