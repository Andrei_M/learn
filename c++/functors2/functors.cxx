#include <cctype>
#include <stdio.h>
#include <iostream>

typedef char (*function)(char);
char callFunction(char (*func)(char), char c)
{
  return func(c);
  // return (*func)(c); // both syntaxes are allowed
}

char switchCase(char c)
{
  char _c = c;
  if (isalpha(_c))
  {
     if (islower(_c)) {
       _c = toupper(_c);
     } else if (isupper(_c)) {
       _c = tolower(_c);
     }
  }
  return _c;
}

void charStyle()
{
  const char * alphabet = "abcdefghijklmnopqrstuvwxyz\nABCDEFGHIJKLMNOPQRSTUVWXYZ";
  int i=0;
  char c = ' ';
  char _c;
  do {
    c = alphabet[i];
    _c = callFunction(&switchCase, c);
    printf("(%c -> %c) ", c, _c);
    i++; 
  } while (c!='\0'); 
  printf("\n");
}

void stringStyle()
{
  std::string alphabet ("abcdefghij!@#^%$##&*&^(*15649klmnopqrstuvwxyz\nABCDEFGHIJKLMNOPQRSTUVWXYZ");
  char c, _c;
  for (auto it = alphabet.begin();
      it!=alphabet.end();
      ++it) {
    c = *it;
    _c = callFunction(&switchCase, c);
    printf("(%c -> %c) ", c, _c);
  }
  printf("\n");
}

int main()
{
  charStyle();
  printf ("\n");
  stringStyle();
  return 0;
}

