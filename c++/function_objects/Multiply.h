/*
 * Multiply.h
 *
 *  Created on: Jun 21, 2012
 *      Author: test
 */

#ifndef MULTIPLY_H_
#define MULTIPLY_H_

#include "FunctionObject.h"

class Multiply: public FunctionObject
{
public:
  Multiply();
  explicit Multiply(int iValue);
  virtual ~Multiply();

  virtual int operator()(int iValue);
};

#endif /* MULTIPLY_H_ */
