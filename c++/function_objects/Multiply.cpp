/*
 * Multiply.cpp
 *
 *  Created on: Jun 21, 2012
 *      Author: test
 */

#include "Multiply.h"

Multiply::Multiply()
:
  FunctionObject()
{
}

Multiply::Multiply(int iValue)
:
  FunctionObject(iValue)
{
}

Multiply::~Multiply()
{
}

int Multiply::operator ()(int iValue)
{
  return _Value * iValue;
}
