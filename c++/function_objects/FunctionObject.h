/*
 * FunctionObject.h
 *
 *  Created on: Jun 21, 2012
 *      Author: test
 */

#ifndef FUNCTIONOBJECT_H_
#define FUNCTIONOBJECT_H_

#include <string>
#include <iostream>

class FunctionObject
{
public:
  FunctionObject();
  explicit FunctionObject(int iIntValue);
  virtual ~FunctionObject();

  // String representation
  std::string ToString() const;
  void Dump(std::ostream& ioOS) const;
  friend std::ostream& operator << (std::ostream& ioOS, const FunctionObject& iFO);

  // Cast operator (where the magic happens)
  virtual int operator () (void);
  virtual int operator () (int iValue);

protected:
  int _Value;
};

#endif /* FUNCTIONOBJECT_H_ */
