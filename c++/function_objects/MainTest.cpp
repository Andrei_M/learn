/*
 * MainTest.cpp
 *
 * Test with function objects in C++
 *
 *  Created on: Jun 21, 2012
 *      Author: test
 */

#include <iostream>
#include "FunctionObject.h"
#include "Multiply.h"

using namespace std;

int main()
{
  // Create a smart function
  FunctionObject *fo = new FunctionObject(5);
  cout << "fo=" << *fo << endl;

  // cast to int
  int lFoValue;
  lFoValue = (*fo).operator()();
  cout << "lFoValue=" << lFoValue << endl;

  // multiply smart function
  int lValue = 2;
  FunctionObject *multiply = new Multiply(2);
  cout << "fo(" << lValue << ")=" << (*fo)(lValue) << endl;
  cout << *multiply << "*" << lValue << "=" << (*multiply)(lValue) << endl;

  return 0;
}


