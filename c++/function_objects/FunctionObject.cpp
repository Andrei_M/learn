/*
 * FunctionObject.cpp
 *
 *  Created on: Jun 21, 2012
 *      Author: test
 */

#include "FunctionObject.h"
#include <iostream>
#include <sstream>
using namespace std;

FunctionObject::FunctionObject()
:
  _Value(0)
{
}

FunctionObject::FunctionObject(int iIntValue)
:
  _Value(iIntValue)
{
}

FunctionObject::~FunctionObject()
{

}


string FunctionObject::ToString() const
{
  ostringstream oss;
  oss << "[" << _Value << "]";
  return oss.str();
}

void FunctionObject::Dump(ostream& ioOS) const
{
  ioOS << ToString();
}

ostream& operator << (ostream& ioOS, const FunctionObject& iFO)
{
  iFO.Dump(ioOS);
  return ioOS;
}


int FunctionObject::operator ()(void)
{
  return _Value;
}

int FunctionObject::operator()(int iValue)
{
  // simply return input parameter
  return iValue;
}
