################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/AApplication_DocModel.cpp \
../src/AApplication_ValueMapBuilder.cpp \
../src/IApplication_DocModel.cpp \
../src/IApplication_INameable.cpp \
../src/IApplication_ValueMapBuilder.cpp \
../src/SFMeshApplication_ElementMaterialBuilder.cpp \
../src/TDocStd_DocModel.cpp 

CXX_SRCS += \
../src/main.cxx 

OBJS += \
./src/AApplication_DocModel.o \
./src/AApplication_ValueMapBuilder.o \
./src/IApplication_DocModel.o \
./src/IApplication_INameable.o \
./src/IApplication_ValueMapBuilder.o \
./src/SFMeshApplication_ElementMaterialBuilder.o \
./src/TDocStd_DocModel.o \
./src/main.o 

CPP_DEPS += \
./src/AApplication_DocModel.d \
./src/AApplication_ValueMapBuilder.d \
./src/IApplication_DocModel.d \
./src/IApplication_INameable.d \
./src/IApplication_ValueMapBuilder.d \
./src/SFMeshApplication_ElementMaterialBuilder.d \
./src/TDocStd_DocModel.d 

CXX_DEPS += \
./src/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -p -pg -Wall -Wextra -c -fmessage-length=0 -v -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.cxx
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -p -pg -Wall -Wextra -c -fmessage-length=0 -v -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


