/*
 * TDocStdDocModel.h
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#ifndef TDOCSTDDOCMODEL_H_
#define TDOCSTDDOCMODEL_H_

#include "AApplication_DocModel.h"

class TDocStd_DocModel : public AApplication_DocModel
{
public:
  TDocStd_DocModel();
  virtual ~TDocStd_DocModel();
};

#endif /* TDOCSTDDOCMODEL_H_ */
