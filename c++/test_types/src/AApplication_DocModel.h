/*
 * AApplication_DocModel.h
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#ifndef AAPPLICATION_DOCMODEL_H_
#define AAPPLICATION_DOCMODEL_H_

#include <string>
using namespace std;
#include "IApplication_DocModel.h"

class AApplication_DocModel : public IApplication_DocModel
{
public:
  AApplication_DocModel();
  virtual ~AApplication_DocModel();

  string GetName()const;

protected:
  AApplication_DocModel(string theDocModelName);

protected:
  string myName;
};

#endif /* AAPPLICATION_DOCMODEL_H_ */
