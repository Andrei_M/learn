/*
 * ValueMapBuilder.cpp
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#include <iostream>
using namespace std;

#include "IApplication_ValueMapBuilder.h"

IApplication_ValueMapBuilder::IApplication_ValueMapBuilder()
{
  cout << "IApplication_ValueMapBuilder()" << endl;
}

IApplication_ValueMapBuilder::~IApplication_ValueMapBuilder()
{
  cout << "~IApplication_ValueMapBuilder()" << endl;
}
