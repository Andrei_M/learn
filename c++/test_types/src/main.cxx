/*
 * main.cxx
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#include <string.h>

#include <iostream>
using namespace std;

#include "IApplication_INameable.h"
#include "IApplication_DocModel.h"
#include "IApplication_ValueMapBuilder.h"

#include "AApplication_DocModel.h"
#include "AApplication_ValueMapBuilder.h"

#include "SFMeshApplication_ElementMaterialBuilder.h"

#include "TDocStd_DocModel.h"

void printDocModelName(IApplication_INameable* aDocModel);
void testDocModels();
void testValueMapBuilders();

enum TestCase {
  NONE,
  DOCMODELS,
  BUILDERS,
};

TestCase getTestCase(int argc, char* argv[])
{
  TestCase aTestCase = NONE;

  if (argc>0) {
    char* aFirstArg = argv[1];
    if (strcmp(aFirstArg,"-t")==0){
      char* aSecondArg = argv[2];
      if (strcmp(aSecondArg,"builders")== 0) {
        aTestCase = BUILDERS;
      } else if (strcmp(aSecondArg,"docmodels") == 0) {
        aTestCase = DOCMODELS;
      }
    }
  }

  return aTestCase;
}

int main(int argc, char* argv[])
{
  TestCase aTestCase = getTestCase(argc, argv);

  switch(aTestCase) {
  case DOCMODELS:
    testDocModels();
    break;

  case BUILDERS:
    testValueMapBuilders();
    break;

  case NONE: // FALL-THROUGH
  default:
    break;
  }

  // End the application, old-school
  cout << "Press ENTER key to end the application..." << endl;
  cin.get();
  cout << "Application ended." << endl;

  return 0;
}

void testDocModels()
{
  cout << "Objects on stack" << endl;
  cout << "----------------" << endl;
  // The AApplicatoin_DocModel
  AApplication_DocModel aADocModel;
  printDocModelName(&aADocModel);

  // The TDocStd_DocModel
  TDocStd_DocModel aTDocModel;
  printDocModelName(&aTDocModel);
  cout << "----------------" << endl << endl;

  cout << "Objects on heap" << endl;
  cout << "----------------" << endl;
  // Using pointers
  IApplication_INameable* aDocModel;
  aDocModel = new AApplication_DocModel();
  printDocModelName(aDocModel);
  delete aDocModel;

  aDocModel = new TDocStd_DocModel();
  printDocModelName(aDocModel);
  delete aDocModel;
  cout << "----------------" << endl << endl;


  cout << "Temporary objects (Warning: memory leaks!)" << endl;
  cout << "----------------" << endl;
  printDocModelName(new AApplication_DocModel()); // memory leak
  printDocModelName(new TDocStd_DocModel()); // memory leak
  cout << "----------------" << endl << endl;
}

void printDocModelName(IApplication_INameable* aDocModel)
{
  string aDocModelName = aDocModel->GetName();
  cout << "The doc model name is " << aDocModelName << endl;
}

void testValueMapBuilders()
{
  cout << "Builders on stack" << endl;
  cout << "-----------------" << endl;
  SFMeshApplication_ElementMaterialBuilder aElementMaterialBuilder;
  IApplication_DocModel* aDocModel = aElementMaterialBuilder.GetDocModel();
  string aDocModelName = aDocModel->GetName();
  cout << "\tThe value builder retrieves values from the following document: " << aDocModelName << endl;
  cout << "Leaving function" << endl;
}
