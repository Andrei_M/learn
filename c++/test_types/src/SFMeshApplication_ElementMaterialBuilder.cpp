/*
 * SFMeshApplicationElementMaterialBuilder.cpp
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#include "SFMeshApplication_ElementMaterialBuilder.h"

#include <iostream>
using namespace std;

#include "TDocStd_DocModel.h"

//=======================================================================
//function : SFMeshApplicatoin_ElementMaterialBuilder
//purpose  :
//=======================================================================
SFMeshApplication_ElementMaterialBuilder::SFMeshApplication_ElementMaterialBuilder()
:
    AApplication_ValueMapBuilder(new TDocStd_DocModel())
{
  cout << "SFMeshApplication_ElementMaterialBuilder()" << endl;
}

//=======================================================================
//function : ~SFMeshApplicatoin_ElementMaterialBuilder
//purpose  :
//=======================================================================
SFMeshApplication_ElementMaterialBuilder::~SFMeshApplication_ElementMaterialBuilder()
{
  cout << "~SFMeshApplication_ElementMaterialBuilder()" << endl;
}

//=======================================================================
//function : SetDocModel
//purpose  :
//=======================================================================
void SFMeshApplication_ElementMaterialBuilder::SetDocModel(
    TDocStd_DocModel* theDocModel)
{
  myDocModel = theDocModel;
}

//=======================================================================
//function : GetDocModel
//purpose  :
//=======================================================================
TDocStd_DocModel* SFMeshApplication_ElementMaterialBuilder::GetDocModel() const
{
  TDocStd_DocModel* aTDocModel = dynamic_cast<TDocStd_DocModel*>(myDocModel);
  return aTDocModel;
}
