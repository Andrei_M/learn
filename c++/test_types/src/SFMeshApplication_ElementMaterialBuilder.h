/*
 * SFMeshApplicationElementMaterialBuilder.h
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#ifndef SFMESHAPPLICATIONELEMENTMATERIALBUILDER_H_
#define SFMESHAPPLICATIONELEMENTMATERIALBUILDER_H_

#include "AApplication_ValueMapBuilder.h"

class TDocStd_DocModel;

class SFMeshApplication_ElementMaterialBuilder : public AApplication_ValueMapBuilder
{
public:
  SFMeshApplication_ElementMaterialBuilder();
  virtual ~SFMeshApplication_ElementMaterialBuilder();

  virtual TDocStd_DocModel* GetDocModel()const;
  virtual void SetDocModel(TDocStd_DocModel* theDocModel);
};

#endif /* SFMESHAPPLICATIONELEMENTMATERIALBUILDER_H_ */
