/*
 * TDocStdDocModel.cpp
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#include "TDocStd_DocModel.h"

#include <iostream>
using namespace std;

TDocStd_DocModel::TDocStd_DocModel()
:
    AApplication_DocModel("TDocStd_DocModel")
{
  cout << "TDocStd_DocModel()" << endl;
}

TDocStd_DocModel::~TDocStd_DocModel()
{
  cout << "~TDocStd_DocModel()" << endl;
}
