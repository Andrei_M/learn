/*
 * IApplicationDocModel.cpp
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#include <iostream>
using namespace std;

#include "IApplication_DocModel.h"

//=======================================================================
//function : IApplication_DocModel
//purpose  :
//=======================================================================
IApplication_DocModel::IApplication_DocModel()
{
  cout << "IApplicatoin_DocModel()" << endl;
}

//=======================================================================
//function : IApplication_DocModel
//purpose  :
//=======================================================================
IApplication_DocModel::IApplication_DocModel(IApplication_DocModel const& other)
{
  cout << "IApplicatoin_DocModel(IApplication_DocModle const&)" << endl;
}

//=======================================================================
//function : ~IApplication_DocModel
//purpose  :
//=======================================================================
IApplication_DocModel::~IApplication_DocModel()
{
  cout << "~IApplication_DocModel()" << endl;
}
