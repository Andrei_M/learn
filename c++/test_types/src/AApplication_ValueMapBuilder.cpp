/*
 * AApplicationValueMapBuilder.cpp
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#include "AApplication_ValueMapBuilder.h"

#include <iostream>
using namespace std;

#include "AApplication_DocModel.h"

//=======================================================================
//function : ValueMapBuilder
//purpose  :
//=======================================================================
AApplication_ValueMapBuilder::AApplication_ValueMapBuilder(
    IApplication_DocModel* theDocModel)
{
  cout << "AApplication_ValueMapBuilder()" << endl;
  SetDocModel(theDocModel);
}

//=======================================================================
//function : ~AApplication_ValueMapBuilder
//purpose  :
//=======================================================================
AApplication_ValueMapBuilder::~AApplication_ValueMapBuilder()
{
  cout << "~AApplication_ValueMapBuilder()" << endl;
  if (myDocModel) {
    delete myDocModel;
  }
  myDocModel = NULL;
}

//=======================================================================
//function : GetDocModel
//purpose  :
//=======================================================================
IApplication_DocModel* AApplication_ValueMapBuilder::GetDocModel() const
{
  cout << "AApplication_ValueMapBuilder::GetDocModel()" << endl;
  return myDocModel;
}

//=======================================================================
//function : FeatureIsSet
//purpose  :
//=======================================================================
void AApplication_ValueMapBuilder::SetDocModel(
    IApplication_DocModel* theDocModel)
{
  myDocModel = theDocModel;
}
