/*
 * ValueMapBuilder.h
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#ifndef VALUEMAPBUILDER_H_
#define VALUEMAPBUILDER_H_

class IApplication_DocModel;

class IApplication_ValueMapBuilder
{
public:
  IApplication_ValueMapBuilder();
  virtual ~IApplication_ValueMapBuilder();

  virtual IApplication_DocModel* GetDocModel()const=0;
};

#endif /* VALUEMAPBUILDER_H_ */
