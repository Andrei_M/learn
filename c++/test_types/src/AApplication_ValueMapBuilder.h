/*
 * AApplicationValueMapBuilder.h
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#ifndef AAPPLICATIONVALUEMAPBUILDER_H_
#define AAPPLICATIONVALUEMAPBUILDER_H_

#include "IApplication_ValueMapBuilder.h"

class IApplication_DocModel;

class AApplication_ValueMapBuilder: public IApplication_ValueMapBuilder
{
public:
  AApplication_ValueMapBuilder(IApplication_DocModel* theDocModel);
  virtual ~AApplication_ValueMapBuilder();

  virtual void SetDocModel(IApplication_DocModel* theDocModel);
  virtual IApplication_DocModel* GetDocModel() const;

protected:
  IApplication_DocModel* myDocModel;
};

#endif /* AAPPLICATIONVALUEMAPBUILDER_H_ */
