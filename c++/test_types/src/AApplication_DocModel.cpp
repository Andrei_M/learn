/*
 * AApplication_DocModel.cpp
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#include "AApplication_DocModel.h"

#include <iostream>
using namespace std;

AApplication_DocModel::AApplication_DocModel()
:
    myName("AApplication_DocModel")
{
  cout << "AApplication_DocModel()" << endl;
}

AApplication_DocModel::AApplication_DocModel(string theName)
:
    myName(theName)
{
  cout << "AApplication_DocModel(string)" << endl;
}


AApplication_DocModel::~AApplication_DocModel()
{
  cout << "~AApplication_DocModel()" << endl;
}

string AApplication_DocModel::GetName() const
{
  cout << "AApplication_DocModel::GetName()" << endl;
  return myName;
}
