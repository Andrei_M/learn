/*
 * IApplicationDocModel.h
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#ifndef IAPPLICATIONDOCMODEL_H_
#define IAPPLICATIONDOCMODEL_H_

#include "IApplication_INameable.h"

class IApplication_DocModel : public IApplication_INameable
{
public:
  IApplication_DocModel();
  IApplication_DocModel(const IApplication_DocModel & other);
  virtual ~IApplication_DocModel();
};

#endif /* IAPPLICATIONDOCMODEL_H_ */
