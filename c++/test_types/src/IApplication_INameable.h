/*
 * IApplication_INameable.h
 *
 *  Created on: Apr 10, 2012
 *      Author: test
 */

#ifndef IAPPLICATIONINAMEABLE_H_
#define IAPPLICATIONINAMEABLE_H_

#include <string>
using namespace std;

class IApplication_INameable
{
public:
  IApplication_INameable();
  virtual ~IApplication_INameable();

  virtual string GetName()const=0;
};

#endif /* IAPPLICATIONINAMEABLE_H_ */
