/*
 * MainTest.cpp
 *
 *  Created on: Jun 21, 2012
 *      Author: test
 */

// system includes
#include <iostream>

// user includes
#include "MyClass.h"
#include "Dog.h"
#include "DogBreed.h"

using namespace std;
using namespace Animals;

int main()
{
  cout << "Hello main" << endl;
  MyClass mc;
  cout << "mc=" << mc << endl;
  mc.EmptyMethod();

  Dog dog(DogBreed_Akita_Inu, "George");
  cout << dog.ToString() << endl;

  return 0;
}

