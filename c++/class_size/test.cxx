#include <iostream>
#include "MyEmptyClass.h"
#include "MyEmptyChildClass.h"
#include "MySimpleClass.h"
#include "MyMethodsClass.h"

using namespace std;

int main(int argc, char* argv[])
{
	MySimpleClass mySimpleClass;
	MyMethodsClass myMethodsClass;
	
	cout << "The size of the classes: " << endl;
	cout << "MyEmptyClass = " << sizeof(MyEmptyClass) << endl;
		cout << "MyEmptyChildClass = " << sizeof(MyEmptyChildClass) << endl;
	cout << "MySimpleClass = " << sizeof(MySimpleClass) << endl;
	cout << "MyMethodsClass = " << sizeof(MyMethodsClass) << endl;
	
	return 0;
}
