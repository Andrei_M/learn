/*
 * MyClass.cpp
 *
 *  Created on: Jun 21, 2012
 *      Author: test
 */

#include "MyClass.h"
#include <sstream>
using namespace std;

MyClass::MyClass()
:
    _DumbValue(-4)
{
}

string MyClass::ToString() const
{
  ostringstream oss;
  oss << "[" << _DumbValue << "]";
  return oss.str();
}

void MyClass::Dump(ostream& os)const
{
  os << ToString();
}

ostream& operator << (ostream& os, const MyClass& instance)
{
  instance.Dump(os);
  return os;
}

void MyClass::EmptyMethod()const
{
  cout << "MyClass::EmptyMethod:IN" << endl;
  cout << "MyClass::EmptyMethod:OUT" << endl;
}
