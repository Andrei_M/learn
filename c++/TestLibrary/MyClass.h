/*
 * MyClass.h
 *
 *  Created on: Jun 21, 2012
 *      Author: test
 */

#include <iostream>

#ifndef MYCLASS_H_
#define MYCLASS_H_

class MyClass
{
public:
  MyClass();

  std::string ToString()const;
  void Dump(std::ostream& os)const;
  friend std::ostream& operator << (std::ostream& os, const MyClass& instance);

  // undefined symbol?
  void EmptyMethod()const;

private:
  int _DumbValue;
};

#endif /* MYCLASS_H_ */
