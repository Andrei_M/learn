/*
 * Breed.h
 *
 *  Created on: Jun 21, 2012
 *      Author: test
 */

#ifndef DOG_BREED_H_
#define DOG_BREED_H_

namespace Animals {
enum DogBreed {
  DogBreed_Unknown=0,
  DogBreed_Akita_Inu=1,
  DogBreed_Afghan_Hound=2,
  DogBreed_Spanish_Water_Dog=3,
};
}

#endif /* DOG_BREED_H_ */
