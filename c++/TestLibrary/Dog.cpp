/*
 * Dog.cpp
 *
 *  Created on: Jun 21, 2012
 *      Author: test
 */

#include <sstream>
#include "Dog.h"
using namespace Animals;
using namespace std;

Dog::Dog()
:
    _Breed(DogBreed_Unknown),
    _Name("")
{
}

Dog::Dog(DogBreed iBreed, string iName)
:
    _Breed(iBreed),
    _Name(iName)
{
}

string Dog::ToString() const
{
  ostringstream oss;
  oss << "[";
  oss << "Breed=" << _Breed;
  oss << "; ";
  oss << "Name=" << _Name;
  oss << "]";
  return oss.str();
}

void Dog::Dump(ostream& os)const
{
  os << ToString();
}

ostream& operator << (ostream& os, const Dog& instance)
{
  instance.Dump(os);
  return os;
}
