/*
 * Dog.h
 *
 *  Created on: Jun 21, 2012
 *      Author: test
 */

#include "DogBreed.h"
#include <string>

#ifndef DOG_H_
#define DOG_H_

namespace Animals{
class Dog
{
public:
  Dog();
  Dog(DogBreed iDogBreed, std::string iName);

  std::string ToString()const;
  void Dump(std::ostream& os)const;
  friend std::ostream& operator << (std::ostream& os, const Dog& instance);

private:
  DogBreed _Breed;
  std::string _Name;
};
}

#endif /* DOG_H_ */
