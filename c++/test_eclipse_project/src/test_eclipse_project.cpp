//============================================================================
// Name        : test_eclipse_project.cpp
// Author      : Andrei
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

#include "Ball.h"
using namespace sport;

int main()
{
  cout << "main:IN" << endl; // prints Hello World!!!
  Ball aBall;
  Ball aBall2(aBall);
  Ball aBall3;
  aBall3 = aBall2;
  cout << "main:OUT" << endl; // prints Hello World!!!
  return 0;
}
