/*
 * Ball.cpp
 *
 *  Created on: Apr 5, 2012
 *      Author: test
 */

#include <iostream>
using namespace std;

#include "Ball.h"

namespace sport {

//=======================================================================
//function : Ball
//purpose  : Plain constructor
//=======================================================================
Ball::Ball()
{
  static int count = 0;
  cout << "Ball constructor (count=" << ++count << ")" << endl;
}

//=======================================================================
//function : Ball
//purpose  : Copy constructor
//=======================================================================
Ball::Ball(const Ball& otherBall)
{
  static int count = 0;
  cout << "Ball Copy Constructor (count=" << ++count << ")" << endl;
}

//=======================================================================
//function : operator =
//purpose  : Copy constructor
//=======================================================================
Ball& Ball::operator=(const Ball& rhs)
{
  static int count = 0;
  cout << "Ball operator = (count=" << ++count << ")" << endl;
  return *this;
}

//=======================================================================
//function : ~Ball
//purpose  :
//=======================================================================
Ball::~Ball()
{
  static int count = 0;
  cout << "Ball destructor (count=" << ++count << ")" << endl;
}

} /* namespace sport */
