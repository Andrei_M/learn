/*
 * Ball.h
 *
 *  Created on: Apr 5, 2012
 *      Author: test
 */

#ifndef BALL_H_
#define BALL_H_

namespace sport {

class Ball {
public:
	Ball();
	Ball(const Ball& otherBall);
	Ball& operator = (const Ball& rhs);
	virtual ~Ball();
};

} /* namespace sport */
#endif /* BALL_H_ */
