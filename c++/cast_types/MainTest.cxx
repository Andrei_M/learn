/**
 * Check values got after converting between types (e.g. float->int)
 */

#include <iostream>
using namespace std;

int main ()
{
  double d1 = 0.9 + .5;
  int i1 = (int) d1;
  cout << "f1 = " << d1 << endl;
  cout << "i1 = " << i1 << endl;
  cout << "------------------------" << endl;
  double d2 = 0.19999999;
  int i2 = d2 * 255;
  int i22 = d2 * 255 + 0.5;
  cout << "d2  = " << d2 << endl;
  cout << "i2  = " << i2 << endl;
  cout << "i22 = " << i22 << endl;
  cout << "------------------------" << endl;
  int i3 = 254;
  double d3 = i3 / 255.;
  int i32 = d3 * 255;
  cout << "i3 = " << i3 << endl;
  cout << "d3 = " << d3 << endl;
  cout << "i32= " << i32 << endl;
  cout << "------------------------" << endl;
}
