/*
 * BaseException.hpp
 *
 *  Created on: Jun 19, 2012
 *      Author: test
 */

#ifndef BASEEXCEPTION_HPP_
#define BASEEXCEPTION_HPP_

class BaseException
{
public:
  BaseException();
  virtual ~BaseException();
  virtual void Raise();
};

#endif /* BASEEXCEPTION_HPP_ */
