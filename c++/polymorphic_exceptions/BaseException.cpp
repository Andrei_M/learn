/*
 * BaseException.cpp
 *
 *  Created on: Jun 19, 2012
 *      Author: test
 */

#include "BaseException.hpp"

BaseException::BaseException()
{
}

void BaseException::Raise()
{
  throw *this;
}

BaseException::~BaseException()
{

}
