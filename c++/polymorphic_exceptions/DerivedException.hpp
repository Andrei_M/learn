/*
 * DerivedException.hpp
 *
 *  Created on: Jun 19, 2012
 *      Author: test
 */

#ifndef DERIVEDEXCEPTION_HPP_
#define DERIVEDEXCEPTION_HPP_

#include "BaseException.hpp"

class DerivedException: public BaseException
{
public:
  DerivedException();
  virtual ~DerivedException();
  virtual void Raise();
};

#endif /* DERIVEDEXCEPTION_HPP_ */
