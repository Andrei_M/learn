/*
 * MainTest.cxx
 *
 * Play with exception types. See what gets caught when you throw exceptions that
 * are part of a hierarchy.
 *  ---------------
 *  |BaseException|
 *  ---------------
 *        ^
 *        |
 * ------------------
 * |DerivedException|
 * ------------------
 *
 * ErrorProneFunction throws either a BaseException or a DerivedException.
 *
 *  Created on: Jun 19, 2012
 *      Author: test
 */

#include <stdlib.h>
#include <time.h>
#include <iostream>
#include "BaseException.hpp"
#include "DerivedException.hpp"

using namespace std;

int ErrorProneFunction();
int main(void)
{
  srand(time(0));

  try {
    int returnCode = ErrorProneFunction();
    if (returnCode != 0) {
      cout << "Still something went wrong. Clean the hamster machine, please do so!" << endl;
    }
  } catch (const DerivedException& de) {
    cout << "DerivedException raised. Nice one :]" << endl;
  } catch (const BaseException& be) {
    cout << "BaesException raised. Fair." << endl;
  } catch (...) {
    cout << "Unknown exception raised. Haaaaalt!" << endl;
    terminate();
  }

  return 0;
}


int ErrorProneFunction()
{
  int exceptionType = rand() % 3;
  if (exceptionType == 0) {
    BaseException e;
    e.Raise();
  } else if (exceptionType == 1){
    DerivedException e;
    e.Raise();
  }

  // Successful
  return 1;
}

