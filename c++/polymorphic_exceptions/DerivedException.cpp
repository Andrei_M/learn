/*
 * DerivedException.cpp
 *
 *  Created on: Jun 19, 2012
 *      Author: test
 */

#include "DerivedException.hpp"

DerivedException::DerivedException()
{
}

void DerivedException::Raise()
{
  throw *this;
}

DerivedException::~DerivedException()
{

}
