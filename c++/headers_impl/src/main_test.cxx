/*
 * main_test.cxx
 *
 *  Created on: May 3, 2012
 *      Author: test
 */

#include <iostream>
#include "header.hxx"

using namespace std;

int main()
{
  int retValue = method(5);
  cout << "The return value of method is: " << retValue << endl;
  return 0;
}
