void  Function(int x, float y, bool b) { }

int x;

int   F1()  {x+=2; return x;}
int   F2()  {x*=2; return x;}
int   F3()  {x+=4; return x;}

int main(int argc, char* argv[])
{
    x = 0;
	
    Function(F1(), F2(), F3());
    
    printf("%d", x);
}
