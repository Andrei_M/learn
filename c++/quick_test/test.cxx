#include <iostream>
using namespace std;

int main(int argc, char*argv[])
{
  int aNegValue = -1;
  int aNilValue = 0;
  int aPosValue = 1;
  
  cout << "aNegValue=" << aNegValue << "; !aNegValue=" << (!aNegValue) << endl;
  cout << "aNilValue=" << aNilValue << "; !aNilValue=" << (!aNilValue) << endl;
  cout << "aPosValue=" << aPosValue << "; !aPosValue=" << (!aPosValue) << endl;

  cout << "!aNegValue != -1 =" << ( ! aNegValue != -1 ) << endl;
  cout << "!aNilValue != -1 =" << ( ! aNilValue != -1 ) << endl;
  cout << "!aPosValue != -1 =" << ( ! aPosValue != -1 ) << endl;
  return 0;
}
