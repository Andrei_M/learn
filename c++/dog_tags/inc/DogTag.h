#ifndef DOG_TAG_H
#define DOG_TAG_H

class DogTag {
public:
  DogTag() : _this(this) {}
  DogTag(const DogTag& rhs) : _this(this)
  {assert(rhs.isValid());}
  ~DogTag() {assert(isValid()); _this=0;}

  DogTag& operator=(const DogTag& rhs)
  {
    assert(isValid());
    assert(rhs.isValid());

    return *this;
  }

  bool isValid() const
  {return _this == this;}

private:
  const DogTag *_this;
};

#endif // DOG_TAG_H
