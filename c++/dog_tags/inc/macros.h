#include <assert.h>

#ifndef DECL_DOG_TAG
#  define DECL_DOG_TAG(dogTag) DogTag dogTag;
#endif // DECL_DOG_TAG
#ifndef CHECK_DOG_TAG
#  define CHECK_DOG_TAG(dogTag) \
     assert (dogTag.isValid())
#endif // CHECK_DOG_TAG
