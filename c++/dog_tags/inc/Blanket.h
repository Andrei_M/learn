#include "macros.h"
#include "DogTag.h"

/**
 * class Blanket (plapuma)
 * Purpose (ro-RO joke): nu te intinde mai mult decat 'ti-e plapuma.
 *
 */
class Blanket
{
public:
  Blanket() : _i(0), _p(0) {}
  int i() const     {return _i;}
  void setI(int i)  {_i = i;}
  int* p() const    {return _p;}
  void setP(int *p) {if (_p) {delete _p; _p=0;} _p=p;}

  // TODO: add method using CHECK_DOG_TAG

private:
  DECL_DOG_TAG(dogTagBegin) // note lack of ;(semicolon). We don't want a `;' in realease code.
  int _i;
  int *_p;
  DECL_DOG_TAG(dogTagEnd)
};
