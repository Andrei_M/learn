#include <vector>
#include <iostream>
#include "macros.h"

int main()
{
  std::vector<int> v;
  v.push_back(3);
  v.push_back(5);
  v.push_back(-1);
  v.push_back(61);
  int i;
  foreach (i, v) {
    std::cout << "i=" << i << std::endl;
  }
  return 0;
}

