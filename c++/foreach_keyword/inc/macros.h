#ifndef MACROS_H
#define MACROS_H

#ifndef foreach
#  define foreach MY_FOREACH
#endif

#define MY_FOREACH(variable, container) \
  for (const MyForeachContainerBase &_container_ = myForeachContainerNew(container);              \
      myForeachContainer(&_container_, true ? 0 : myForeachPointer(container))->condition();      \
      ++myForeachContainer(&_container_, true ? 0 : myForeachPointer(container))->i)              \
    for (variable = *myForeachContainer(&_container_, true ? 0 : myForeachPointer(container))->i; \
        myForeachContainer(&_container_, true ? 0 : myForeachPointer(container))->brk;            \
        --myForeachContainer(&_container_, true ? 0 : myForeachPointer(container))->brk)          \


struct MyForeachContainerBase {};

template <typename T>
class MyForeachContainer : public MyForeachContainerBase {
public:
  inline MyForeachContainer(const T& t)
  : c(t), brk(0), i(c.begin()), e(c.end()) {}

  inline bool condition() const { return (!brk++ && i != e); }

  const T c;
  mutable int brk;
  mutable typename T::const_iterator i, e;
};

template <typename T> 
inline T *myForeachPointer(const T &) 
{return 0;}

template <typename T>
inline MyForeachContainer<T> myForeachContainerNew(const T& t)
{return MyForeachContainer<T>(t);}

template <typename T>
inline const MyForeachContainer<T> *myForeachContainer(const MyForeachContainerBase * base, const T*)
{return static_cast<const MyForeachContainer<T> *>(base);}

#endif // MACROS_H
