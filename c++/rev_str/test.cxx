#include <iostream>
#include <sstream>
#include <string>

using namespace std;

class StringTools
{
public:
  static string Reverse(const string& s)
  {
    stringstream ss;
    rev_str_rec(s.c_str(), ss);
    return ss.str();
  }

private:
  static void rev_str_rec(const char* s, stringstream& ss)
  {
    if(*s != '\0')
         rev_str_rec(s+1, ss);

    ss << *(s);
  }
};

int main()
{
  string s = "born2c0de";
  
  string rs = StringTools::Reverse(s);
  cout << s << " - " << rs << endl;

  return 0;
}
