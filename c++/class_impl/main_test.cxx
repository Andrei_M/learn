/*
 * main_test.cxx
 *
 *  Created on: May 3, 2012
 *      Author: test
 */

#include <iostream>
#include "TestClass.h"

using namespace std;

int main(void)
{
  TestClass aTestClass;
  int aValue = 5;
  int aTwiceValue = aTestClass.Twice(aValue);
  cout << "Twice as much as " << aValue << " is " << aTwiceValue << endl;

  return 0;
}

