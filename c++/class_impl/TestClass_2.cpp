/*
 * TestClass.cpp
 *
 *  Created on: May 3, 2012
 *      Author: test
 */

#include "TestClass.h"

#include <iostream>

using namespace std;

int TestClass::Twice(int arg)
{
  return 2*arg;
}
