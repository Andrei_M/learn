/*
 * TestClass.cpp
 *
 *  Created on: May 3, 2012
 *      Author: test
 */

#include "TestClass.h"

#include <iostream>

using namespace std;

TestClass::TestClass()
{
  cout << "TestClass::TestClass()" << endl;
}

TestClass::~TestClass()
{
  cout << "TestClass::~TestClass()" << endl;
}
