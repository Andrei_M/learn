/*
 * TestClass.h
 *
 *  Created on: May 3, 2012
 *      Author: test
 */

#ifndef TESTCLASS_H_
#define TESTCLASS_H_

class TestClass
{
public:
  TestClass();
  int Twice(int);
  virtual ~TestClass();
};

#endif /* TESTCLASS_H_ */
