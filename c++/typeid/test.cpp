/*#include <typeinfo>
#include <iostream>
using namespace std;

class MyBaseClass{};
class MyDerivedClass : public MyBaseClass {};

int main(int argc, char* argv[])
{
  int aInt;
  MyBaseClass aBC;
  MyDerivedClass aDC;
  MyBaseClass* aBDC = new MyDerivedClass();
  
  cout << "aInt type: " << typeid(aInt).name() << endl;
  cout << "aBC  type: " << typeid(aBC).name()  << endl;
  cout << "aBD  type: " << typeid(aDC).name()  << endl;
  cout << "aBCD type: " << typeid(*aBDC).name() << endl;
  
  return 0;
}*/

// type_info example
#include <iostream>
#include <typeinfo>
using namespace std;

struct Base {};
struct Derived : Base {};
struct Poly_Base {virtual void Member(){}};
struct Poly_Derived: Poly_Base {};

int main() {
  // built-in types:
  int i;
  int * pi;
  cout << "int is: " << typeid(int).name() << endl;
  cout << "  i is: " << typeid(i).name() << endl;
  cout << " pi is: " << typeid(pi).name() << endl;
  cout << "*pi is: " << typeid(*pi).name() << endl << endl;

  // non-polymorphic types:
  Derived derived;
  Base* pbase = &derived;
  cout << "derived is: " << typeid(derived).name() << endl;
  cout << " *pbase is: " << typeid(*pbase).name() << endl;
  cout << boolalpha << "same type? "; 
  cout << ( typeid(derived)==typeid(*pbase) ) << endl << endl;

  // polymorphic types:
  Poly_Derived polyderived;
  Poly_Base* ppolybase = &polyderived;
  cout << "polyderived is: " << typeid(polyderived).name() << endl;
  cout << " *ppolybase is: " << typeid(*ppolybase).name() << endl;
  cout << boolalpha << "same type? "; 
  cout << ( typeid(polyderived)==typeid(*ppolybase) ) << endl << endl;
  
  return 0;
}
