/*
 * MyClass.cpp
 *
 *  Created on: Nov 28, 2012
 *      Author: test
 */

#include "MyClass.h"
#include <iostream>
using namespace std;

namespace domain
{

MyClass::MyClass()
:
    _myXField(-1)
{
  cout << "MyClass::MyClass()" << endl;
}

void MyClass::TooLateForMe() const
{
  if (this == 0) {
    cout << "So, this is NULL... YAY!" << endl;
//    cout << "x=" << _myXField << endl; // don't enable this line: it will cause terrific problems
    return;
  }

  cout << "Too bad, this is still amongst us" << endl;
  cout << "x=" << _myXField << endl;
}



} /* namespace domain */
