/*
 * MyClass.h
 *
 *  Created on: Nov 28, 2012
 *      Author: test
 */

#ifndef MYCLASS_H_
#define MYCLASS_H_

namespace domain
{

class MyClass
{
public:
  MyClass();

  void TooLateForMe() const;

private:
  int _myXField;
};

} /* namespace domain */
#endif /* MYCLASS_H_ */
