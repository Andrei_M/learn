Standard_CString pad(
    Standard_Integer   theNumberToPad,
    Standard_Integer   theTotalNumberOfPositions,
    Standard_Character thePaddingCharacter)
{
  stringstream ss;
  Standard_Integer aDigitCount = 1;
  Standard_Integer aNumberCopy = theNumberToPad;
  while (aNumberCopy != 0) {
    aNumberCopy /= 10;
    if (aNumberCopy != 0) {
      aDigitCount += 1;
    }
  }

  while (theTotalNumberOfPositions - aDigitCount > 0) {
    ss << thePaddingCharacter;
    aDigitCount += 1;
  }

  ss << theNumberToPad;

  return ss.str().c_str();
}

Standard_CString pad0(Standard_Integer theNumberToPad)
{
  return pad(theNumberToPad, 3, '0');
}
