/*
 * MainTest.cxx
 *
 * Test bit operators
 *
 *  Created on: Aug 23, 2012
 *      Author: test
 */
#include <iostream>
using namespace std;

int main ()
{
  int rgb = 0xffffffff + 1;

  int r = (rgb>>16) & 0xff;
  int g = (rgb>>8)  & 0xff;
  int b = (rgb)     & 0xff;

  cout << "rgb=" << rgb << endl;
  cout << "r=" << r << endl;
  cout << "g=" << g << endl;
  cout << "b=" << b << endl;

  return 0;
}


