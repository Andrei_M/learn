/*
 * Object.cxx
 *
 *  Created on: Jun 7, 2012
 *      Author: test
 */

#include <iostream>
#include "Object.hxx"
#include "CriticalException.hxx"

using namespace std;

Object::Object()
{
  // TODO Auto-generated constructor stub
  cout << "Object::Object" << endl;
}

void Object::DoSensitiveOperation(Object* theSensitiveArgument)
{
  cout << "Watch out, things are getting hot in here!" << endl;

  if (theSensitiveArgument == 0) {
    throw CriticalException("the very important argument is NULL! Can't perform sensitive operation");
  }

  cout << "Phewee, just came from a very delicate job. Smooth operator, me!" << endl;
}

