/*
 * Object.hxx
 *
 *  Created on: Jun 7, 2012
 *      Author: test
 */

#ifndef OBJECT_HXX_
#define OBJECT_HXX_

class Object
{
public:
  Object();
  void DoSensitiveOperation(Object*);
};

#endif /* OBJECT_HXX_ */
