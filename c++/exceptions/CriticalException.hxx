/*
 * CriticalException.h
 *
 *  Created on: Jun 7, 2012
 *      Author: test
 */

#ifndef CRITICALEXCEPTION_HXX_
#define CRITICALEXCEPTION_HXX_

#include <string>
using namespace std;

class CriticalException
{
public:
  CriticalException(string theMessage);
  CriticalException(const CriticalException& other);
  string ToString() const;

private:
  string myMessage;
};

#endif /* CRITICALEXCEPTION_HXX_ */
