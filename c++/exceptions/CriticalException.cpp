/*
 * CriticalException.cpp
 *
 *  Created on: Jun 7, 2012
 *      Author: test
 */

#include "CriticalException.hxx"
#include <iostream>
using namespace std;

CriticalException::CriticalException(string theMessage)
:
    myMessage(theMessage)
{
  cout << "CriticalException::CriticalException" << endl;
}

CriticalException::CriticalException(const CriticalException& other)
{
  cout << "CriticalException::CriticalException<copy_constructor>" << endl;

  // and just out of a sudden...
  throw "you\'re screwed";
}

string CriticalException::ToString() const
{
  return myMessage;
}
