/*
 * MainTest.cxx
 *
 *  Created on: Jun 7, 2012
 *      Author: test
 *
 * Lesson: Throw erros by value
 * Not by pointer, especially not by allocating space for that exception object.
 * How can you throw by pointer, when the error you want to signal is "OutOfMemory"?
 * You say that you don't have any memory left, but you try to allocate a new
 * object just to tell that! Ouch!
 * Moreover, by throwing by pointer, you put the heavy work on the client's
 * shoulder of de-allocating the memory at the catch site!
 *
 * Lesson: Catch by reference
 * Short story: the copy constructor is not called!
 */
#include <iostream>
#include "Object.hxx"
#include "CriticalException.hxx"

using namespace std;

int main(int argc, char* argv[])
{
  cout << "Hello exceptions" << endl;
  Object aObject;
  try {
  aObject.DoSensitiveOperation(0);
  } catch (CriticalException& e) {
    cout << "Error detected!" << endl;
    cout << "Error message: " << e.ToString() << endl;
  }

  cout << "All good, sir, we are all rich now!" << endl;

  return 0;
}


