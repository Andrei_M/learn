#include <iostream>

using namespace std;

// A simple class
class X
{
public:
  // fn is a simple virtual funciton
  virtual void fn()
  {
    cout << "n = " << n << endl;
  }

  // a member variable
  int n;
};

int main()
{
  // Create an object (obj) of class X
  X * obj = new X();
  obj->n = 10;

  // get the virtual table pointer of object obj
  int* vptr = *(int**)obj;

  // we shall call the functio fn, but first the following assembly code
  // is required ot make obj as 'this' pointer as we shall call
  // function fn() directly from the virtual table
  __asm
  {
    mov ecx, obj
  }

  // function fn is the first entry of the virtual table, so it's vptr[0]
  ((void(*)()) vptr[0])();

  // the above is the same as the following
  // obj->fn();

  return 0;
}
