/*
 * MainTest.cxx
 *
 *  Created on: May 21, 2012
 *      Author: test
 */

#include <iostream>
#include <sstream>

typedef float (*functor)(float, float);
float plus(float, float);
float minus(float, float);
float multiply(float, float);
float divide(float, float);
class Exception {
private:
  std::string myExceptionMessage;

public:
  Exception(const std::string & theExceptionMessage)
  {myExceptionMessage = theExceptionMessage;}
};

class DivisionByZeroException : public Exception{
public:
  DivisionByZeroException(std::string theExceptionMessage):Exception(theExceptionMessage){}
};

float get_operation_result(functor, float, float);
char get_operation_char(functor);
int main(void)
{
  float a = 2.0f;
  float b = 0.5f;

  /* The operations */
  functor functor_array[4];
  functor_array[0] = &plus;
  functor_array[1] = &minus;
  functor_array[2] = &multiply;
  functor_array[3] = &divide;

  for (int i=0; i<4; ++i) {
    float result = get_operation_result(functor_array[i], a, b);
    char operation_char = get_operation_char(functor_array[i]);
    std::cout << a << ' ' << operation_char << ' ' << b << " = " << result << std::endl;
  }
  
  get_operation_result(&plus, a, b);

  return 0;
}

float get_operation_result(functor fn, float a, float b)
{
  return fn(a, b);
}

char get_operation_char(functor fn)
{
  if      (fn==&plus)     return '+';
  else if (fn==&minus)    return '-';
  else if (fn==&multiply) return '*';
  else if (fn==&divide)   return '/';
  else return '\0';
}

float plus(float a, float b)
{
  return a+b;
}

float minus(float a, float b)
{
  return a-b;
}

float multiply(float a, float b)
{
  return a*b;
}

float divide(float a, float b)
{
  if (b == 0) {
    std::stringstream aMessage;
    aMessage << "Cannot divide " << a << " by 0";
    throw new DivisionByZeroException(aMessage.str());
  }

  
  return a/b;
}
