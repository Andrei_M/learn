/*
 * Operation.cpp
 *
 *  Created on: Jun 12, 2012
 *      Author: test
 */

#include "Operation.h"

namespace Geometry
{

//=======================================================================
//function : Constructor
//purpose  :
//=======================================================================
Operation::Operation(Point2D* iPoint)
:
    _Point(iPoint)
{
}

//=======================================================================
//function : Translate
//purpose  :
//=======================================================================
void Operation::Translate(int iXValue, int iYValue)
{
  _Point->X() += iXValue;
  _Point->Y() += iYValue;
}

//=======================================================================
//function : ClampX
//purpose  :
//=======================================================================
void Operation::ClampX(int iLowBound, int iHighBound)
{
  if (_Point->X() < iLowBound) {
    _Point->X() = iLowBound;
  }

  if (_Point->X() > iHighBound) {
    _Point->X() = iHighBound;
  }
}

//=======================================================================
//function : ClampY
//purpose  :
//=======================================================================
void Operation::ClampY(int iLowBound, int iHighBound)
{
  if (_Point->Y() < iLowBound) {
    _Point->Y() = iLowBound;
  }

  if (_Point->Y() > iHighBound) {
    _Point->Y() = iHighBound;
  }
}


} /* namespace Geometry */
