/*
 * Main.cxx
 *
 *  Created on: Jun 12, 2012
 *      Author: test
 */

#include <iostream>
#include "Point2D.h"
#include "Operation.h"

using namespace std;

int main(int argc, char* argv[])
{
  Geometry::Point2D lPoint (2, 5);
  cout << lPoint << endl;

  Geometry::Operation lOperation(&lPoint);

  int lNbrSteps = 10000;
  for (int i=0; i<lNbrSteps; ++i) {
    lOperation.Translate(1, 0);
    lOperation.ClampX(0, 1024);
    cout << lPoint << endl;
  }
}

