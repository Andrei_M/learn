/*
 * Point2D.cpp
 *
 *  Created on: Jun 12, 2012
 *      Author: test
 */

#include "Point2D.h"
#include <sstream>

namespace Geometry
{

//=======================================================================
//function : Point2D
//purpose  :
//=======================================================================
Point2D::Point2D()
:
    _X(0),
    _Y(0)
{
}

//=======================================================================
//function : Point2D
//purpose  :
//=======================================================================
Point2D::Point2D(int iX, int iY)
:
    _X(iX),
    _Y(iY)
{

}

//=======================================================================
//function : operator <<
//purpose  :
//=======================================================================
std::ostream& operator << (std::ostream& os, const Point2D& iPoint)
{
  iPoint.Dump(os);
  return os;
}

//=======================================================================
//function : Dump
//purpose  :
//=======================================================================
void Point2D::Dump(std::ostream& iOS) const
{
  iOS << this->ToString();
}

//=======================================================================
//function : ToString
//purpose  :
//=======================================================================
std::string Point2D::ToString() const
{
  std::stringstream lStringStream;
  lStringStream << "(" << _X << "," << _Y << ")";

  return lStringStream.str();
}

} /* namespace Geometry */
