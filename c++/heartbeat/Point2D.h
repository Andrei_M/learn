/*
 * Point2D.h
 *
 *  Created on: Jun 12, 2012
 *      Author: test
 */

#ifndef POINT2D_H_
#define POINT2D_H_

#include <iostream>
#include <string>

namespace Geometry
{

class Point2D
{
public:
  Point2D();
  Point2D(int iX, int iY);

  // X
  int  X() const    { return _X; }
  int& X()          { return _X; }
  void SetX(int iX) { _X = iX;   }

  // Y
  int  Y() const    { return _Y; }
  int& Y()          { return _Y; }
  void SetY(int iY) { _Y = iY;   }

  // text representation
  friend std::ostream& operator << (std::ostream& os, const Point2D& iPoint);
  void Dump(std::ostream& iOS) const;
  std::string ToString() const;

private:
  int _X;
  int _Y;
};

} /* namespace Geometry */
#endif /* POINT2D_H_ */
