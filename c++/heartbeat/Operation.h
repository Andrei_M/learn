/*
 * OPERATION.h
 *
 *  Created on: Jun 12, 2012
 *      Author: test
 */

#ifndef OPERATION_H_
#define OPERATION_H_

#include "Point2D.h"

namespace Geometry
{

class Operation
{
public:
  Operation(Point2D* iPoint);

  Point2D  Point() const;
  Point2D& Point();
  void SetPoint(Point2D* iPoint);

  void Translate(int iX, int iY);

  void ClampX(int iLowBound, int iHighBound);
  void ClampY(int iLowBound, int iHighBound);

private:
  Point2D* _Point;
};

} /* namespace Geometry */
#endif /* OPERATION_H_ */
