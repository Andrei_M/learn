/*
 * graph.hxx
 *
 *  Created on: May 9, 2012
 *      Author: test
 */

#ifndef GRAPH_HXX_
#define GRAPH_HXX_

class Graph
{
private:
  int myID;

public:
  explicit Graph(int theID) : myID(theID){};
  int getID() const { return myID; }
  void setID(int theID) {myID=theID;}
};


#endif /* GRAPH_HXX_ */
