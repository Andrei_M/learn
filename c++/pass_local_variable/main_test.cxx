/*
 * main_test.cxx
 *
 *  Created on: May 9, 2012
 *      Author: test
 */

#include <iostream>
using namespace std;

void displayName(ostream& theStream, const string& theName)
{
  theStream << theName << endl;
}

int main(void)
{
  cout << "Hello project" << endl;
  {
    string aName = "Andrei";
    displayName(cout, aName);
  }
  return 0;
}

