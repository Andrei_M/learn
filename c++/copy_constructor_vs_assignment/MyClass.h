/*
 * MyClass.h
 *
 *  Created on: Jun 14, 2012
 *      Author: test
 */

#ifndef MYCLASS_H_
#define MYCLASS_H_

#include <iostream>

class MyClass
{
public:
  MyClass();
  MyClass& operator=(const MyClass& rhs);

  int GetInt() const { return i; }

  // String representation
  friend std::ostream& operator << (std::ostream& os, const MyClass& instance);
  void Dump(std::ostream& os) const;
  std::string ToString() const;

private:
  MyClass(const MyClass& other);
  int i;
};

#endif /* MYCLASS_H_ */
