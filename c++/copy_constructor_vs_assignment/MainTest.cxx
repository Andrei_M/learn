/*
 * MainTest.cxx
 *
 *  Created on: Jun 14, 2012
 *      Author: test
 */

#include <iostream>

#include "MyClass.h"

using namespace std;

int main(void)
{
  cout << "MainText.main:IN" << endl;
  MyClass a;
//  MyClass b = a; // oops, copy construtor is private
  MyClass c; // ok, uses assignment operator, not the copy constructor

  c = a;
//  cout << "a=" << a << "; b=" << b << "; c=" << c << endl;
  cout << "a=" << a << "; c=" << c << endl;
  cout << "MainText.main:OUT" << endl;
  return 0;
}
