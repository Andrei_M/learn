/*
 * MyClass.cpp
 *
 *  Created on: Jun 14, 2012
 *      Author: test
 */

#include "MyClass.h"
#include <sstream>
using namespace std;

MyClass::MyClass()
:
    i(3)
{
}

MyClass::MyClass(const MyClass& other)
{
  i = other.i;
}

MyClass& MyClass::operator =(const MyClass& rhs)
{
  return *this;
}

ostream& operator << (ostream& os, const MyClass& instance)
{
  instance.Dump(os);
  return os;
}

void MyClass::Dump(ostream& os) const
{
  os << ToString();
}

string MyClass::ToString() const
{
  stringstream ss;
  ss << GetInt();
  return ss.str();
}

