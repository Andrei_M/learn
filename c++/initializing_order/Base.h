/*
 * Base.h
 *
 *  Created on: Jun 14, 2012
 *      Author: test
 */

#ifndef BASE_H_
#define BASE_H_

class Base
{
public:
  Base(int iBInt);

  int GetInt() const { return _BInt; }

private:
  int _BInt;
};

#endif /* BASE_H_ */
