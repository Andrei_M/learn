/*
 * MainTest.cxx
 *
 *  Created on: Jun 14, 2012
 *      Author: test
 */

#include <iostream>
#include <string>

#include "Base.h"
#include "Derived.h"

using namespace std;

int main(void)
{
  cout << "MainTest.main:IN" << endl;

  // d'abord, create an instance
  Base *b = new Derived();

  cout << "b.GetInt()=" << b->GetInt() << endl;

  // later on...
  delete b;

  cout << "MainTest.main:OUT" << endl;
  return 0;
}


