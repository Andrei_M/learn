/*
 * Derived.h
 *
 *  Created on: Jun 14, 2012
 *      Author: test
 */

#ifndef DERIVED_H_
#define DERIVED_H_

#include "Base.h"
#include <string>

class Derived : public Base
{
public:
  Derived();

private:
  std::string _DString;
};

#endif /* DERIVED_H_ */
