/*
 * MyClass.h
 *
 *  Created on: Jun 14, 2012
 *      Author: test
 */

#ifndef MYCLASS_H_
#define MYCLASS_H_

#include <iostream>
#include <string>

class MyClass
{
public:
  MyClass();
  MyClass(int iValue);
  explicit MyClass(const MyClass& other);

  // String representation
  friend std::ostream& operator << (std::ostream& os, const MyClass& iMyClass);
  void Dump(std::ostream& os) const;
  std::string ToString() const;

private:
  int _Value;
};

#endif /* MYCLASS_H_ */
