/*
 * MainTest.cxx
 *
 * Test if the copy constructor gets called when declaring and initializing
 * an object in the same spot, using another object.
 *
 *  Created on: Jun 14, 2012
 *      Author: test
 */

#include <iostream>
#include "MyClass.h"
using namespace std;

int main(void)
{
  cout << "main:IN" << endl;

  MyClass a(42);
  cout << "a=" << a << endl;

  MyClass b(a); // Explicit call, works
  cout << "b=" << b << endl;

//  MyClass c = a; // Oops, the copy constructor must be explicitly called :-"
//  cout << "c=" << c << endl;

//  MyClass d = 3; // Hmm... doesn't work... 3 is casted to MyClass ?!
//  cout << "d=" << d << endl;

  cout << "main:OUT" << endl;
  return 0;
}

