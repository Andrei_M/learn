/*
 * MyClass.cpp
 *
 *  Created on: Jun 14, 2012
 *      Author: test
 */

#include "MyClass.h"
#include <sstream>
using namespace std;

MyClass::MyClass()
:
    _Value(-1)
{
  cout << "MyClass::MyClass():IN" << endl;
  cout << "MyClass::MyClass():OUT" << endl;
}

MyClass::MyClass(int iValue)
:
    _Value(iValue)
{
  cout << "MyClass::MyClass(int):IN" << endl;
  cout << "MyClass::MyClass(int):OUT" << endl;
}

MyClass::MyClass(const MyClass& other)
{
  cout << "MyClass::<copy_constructor>:IN" << endl;
  _Value = other._Value;
  cout << "MyClass::<copy_constructor>:OUT" << endl;
}

ostream& operator << (ostream& os, const MyClass& iMyClass)
{
  iMyClass.Dump(os);
  return os;
}

void MyClass::Dump(ostream& os) const
{
  os << ToString();
}

string MyClass::ToString() const
{
  stringstream ss;
  ss << "(Value=" << _Value << ")";
  return ss.str();
}
