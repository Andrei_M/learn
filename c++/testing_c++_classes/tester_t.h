#ifndef tester_t_H
#define tester_t_H

#ifdef __GNUC__
#define __cdecl __attribute__((__cdecl__))
#endif

#include <cstdarg>
#include <cassert>

template<typename _C>
struct tester_t
{
  _C& _i;
  const _C& _e;

  tester_t(_C& i, const _C& e)
    :
      _i(i)
      , _e(e)
  { }

  virtual ~tester_t ( ) { }

  bool __cdecl test(const char* name, ... )
  {
    typedef _C& (_C::* _PFMC)( );
    typedef std::pair<_PFMC, std::string> PNFMC;

    workflow<_C> wf;

    va_list ap;
    va_start(ap, name);
    do {
      PNFMC* pfnmf = static_cast<PNFMC*>(va_arg(ap, PNFMC*));
      if (!pfnmf) {
        break;
      } else {
        wf += functor<_C>(pfnmf->first, pfnmf->second);
      }
    } while (true);
    va_end(ap);

    std::cout << "Running test: " << name << std::endl;
    wf(_i);

    return _i == _e;
  }

  void assert_succeeded()
  { assert(_i == _e); }

  void assert_fail( )
  { assert(_i != _e); }

  void assert_ex(const std::string& msg, bool strong)
  {
    if (_i != _e) {
      std::cout << "assertion failure: expected " << _e
        << " but is " << _i
        << " FAILURE" << std::endl;
      if (strong) {
        assert(_i == _e);
      }
    } else {
      std::cout << "assertion passed : expected "
        << _e << " and is " << _i
        << " SUCCESS" << std::endl;
    }
  }
};

#endif // tester_t_H
