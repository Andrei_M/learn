#include <utility>
#include <string>
#include <cstddef>

#include "functor.h"
#include "workflow.h"
#include "tester_t.h"
#include "integer.h"

void test_1();

int main()
{
  test_1(); 

  return 0;
}

void test_1()
{
  integer input(2);
  integer expected(5);

  tester_t<integer> tester(input, expected);
  std::pair<integer& (integer::*)(), std::string> incPair   = std::make_pair<integer& (integer::*)(), std::string>(&integer::inc,   "inc  ");
  std::pair<integer& (integer::*)(), std::string> dmultPair = std::make_pair<integer& (integer::*)(), std::string>(&integer::inc,   "dmult");
  std::pair<integer& (integer::*)(), std::string> decPair   = std::make_pair<integer& (integer::*)(), std::string>(&integer::inc,   "dec  ");

  tester.test( "integer test (((2+1)*2)-1) ==> 5", & incPair, & dmultPair, & decPair, NULL);

  // tester.assert_succeeded();
  tester.assert_ex("input 2 expected 5", false);
  std::cout << std::endl;

  return;
}
