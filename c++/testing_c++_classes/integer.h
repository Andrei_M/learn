#ifndef integer_H
#define integer_H

struct integer
{
  int _n;

  explicit integer(int n=0)
    : _n(n)
  { }

  virtual ~integer( ) { }

  bool operator==(int n) const
  { return _n == n; }

  bool operator==(const integer& right) const
  { return _n == right._n; }

  bool operator!=(int n) const
  { return _n != n; }

  bool operator!=(const integer& right) const
  { return _n != right._n; }

  integer& nop()
  { return *this; }

  integer& inc()
  { ++_n; return *this; }

  integer& dec()
  { --_n; return *this; }

  integer& dmult()
  { _n*=2; return *this; }

  friend std::ostream& operator<<(std::ostream& o, const integer& i)
  { o << i._n; return o; }

};

#endif // integer_H

