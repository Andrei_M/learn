#ifndef workspace_H
#define workspace_H

#include <vector>
#include <iostream>
#include "functor.h"

template<typename _F>
struct workflow
{
  std::vector<functor<_F> > _funcs;

  workflow( ) { }

  virtual ~workflow ( ) { } 

  workflow& operator+=(const functor<_F>& f)
  {
    _funcs.push_back(f);
    return *this;
  }

  _F& operator()(_F& arg)
  {
    typename std::vector<functor<_F> >::iterator itf;
    std::cout << "==> arg=" << arg << std::endl;
    for(itf = _funcs.begin(); itf != _funcs.end(); itf++) {
      std::cout << " --> " << itf->name().c_str() << " arg=" << arg << std::endl;
      (*itf)(arg);
      std::cout << " <-- " << itf->name().c_str() << " arg=" << arg << std::endl;
    }
    
    std::cout << "<== arg=" << arg << std::endl;
    return arg;
  }
};


#endif // workspace_H
