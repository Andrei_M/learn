#ifndef functor_H
#define functor_H

#include <string>

template<typename _F>
struct functor
{
  _F& (_F:: *_pf)();
  std::string _name;

  functor(_F& (_F::*pf)(), const std::string& name)
    :
      _pf(pf)
      , _name(name)
  { }

  functor (const functor& src)
    :
      _pf(src._pf)
      , _name(src._name)
  { }

  virtual ~functor() { }
  
  _F& operator()(_F& arg)
  { return (arg.*_pf)(); }

  const std::string& name() const
  { return _name; }

};

#endif // functor_H
