#include <iostream>
using namespace std;

int main()
{
  int x = 1001;
  cout << "(1)" << x << endl;
  int i=1;
  while(1) {
    x = (x%2==0) ? x/2 : 3*x + 1;
    cout << "(" << ++i << ")" << x << endl;
    if (x==1) {
      break;
    }
  }
  return 0;
}
